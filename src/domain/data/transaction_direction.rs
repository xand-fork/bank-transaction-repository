use xand_banks::models::BankTransactionType;

/// In accounting terms, according to the bank's perspective:
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum TransactionDirection {
    /// Money was leaving this account (being transferred out)
    Debit,
    /// Money was arriving in this account (being transferred in)
    Credit,
}

impl From<BankTransactionType> for TransactionDirection {
    fn from(txn_type: BankTransactionType) -> Self {
        match txn_type {
            BankTransactionType::Debit => Self::Debit,
            BankTransactionType::Credit => Self::Credit,
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::domain::data::TransactionDirection;
    use xand_banks::models::BankTransactionType;

    #[test]
    fn from__bank_transaction_type_debit_resolves_to_transaction_direction_debit() {
        // Given
        let bank_transaction_type = BankTransactionType::Debit;

        // When
        let transaction_direction: TransactionDirection = bank_transaction_type.into();

        // Then
        assert_eq!(transaction_direction, TransactionDirection::Debit);
    }

    #[test]
    fn from__bank_transaction_type_credit_resolves_to_transaction_direction_credit() {
        // Given
        let bank_transaction_type = BankTransactionType::Credit;

        // When
        let transaction_direction: TransactionDirection = bank_transaction_type.into();

        // Then
        assert_eq!(transaction_direction, TransactionDirection::Credit);
    }
}
