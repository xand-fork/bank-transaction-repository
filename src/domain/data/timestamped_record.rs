use crate::domain::data::DomainTransaction;
use chrono::{DateTime, Utc};

#[derive(Clone, Debug, Eq, PartialEq)]
pub struct TimestampedTransaction {
    pub transaction: DomainTransaction,
    pub timestamp: DateTime<Utc>,
}
