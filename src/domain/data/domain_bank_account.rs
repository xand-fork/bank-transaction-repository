use std::hash::Hash;

#[derive(Debug, Clone, Hash, Eq, PartialEq, PartialOrd, Ord)]
pub struct DomainBankAccount {
    routing_number: String,
    account_number: String,
}

impl DomainBankAccount {
    pub const fn new(routing_number: String, account_number: String) -> Self {
        Self {
            routing_number,
            account_number,
        }
    }

    pub fn get_routing_number(&self) -> String {
        self.routing_number.clone()
    }

    pub fn get_account_number(&self) -> String {
        self.account_number.clone()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    impl DomainBankAccount {
        pub fn test() -> Self {
            Self::new("0101010101".to_string(), "8989898989".to_string())
        }
        pub fn from_str(routing: &str, account: &str) -> Self {
            Self::new(routing.into(), account.into())
        }
    }
}
