use chrono::{DateTime, Utc};

use super::DomainBankAccount;

#[derive(Debug)]
pub struct DomainHistoryRequest {
    bank_account: DomainBankAccount,
    history_after: DateTime<Utc>,
}

impl DomainHistoryRequest {
    pub const fn new(bank_account: DomainBankAccount, history_after: DateTime<Utc>) -> Self {
        Self {
            bank_account,
            history_after,
        }
    }

    pub fn get_bank_account(&self) -> DomainBankAccount {
        self.bank_account.clone()
    }

    pub const fn get_history_after(&self) -> DateTime<Utc> {
        self.history_after
    }
}
