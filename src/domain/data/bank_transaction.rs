use crate::TransactionDirection;
use xand_money::Usd;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct BankTransaction {
    bank_provided_id: String,
    amount: Usd,
    credit_or_debit: TransactionDirection,
    metadata: String,
}

impl BankTransaction {
    pub const fn new(
        bank_provided_id: String,
        amount: Usd,
        credit_or_debit: TransactionDirection,
        metadata: String,
    ) -> Self {
        Self {
            bank_provided_id,
            amount,
            credit_or_debit,
            metadata,
        }
    }

    pub fn bank_provided_id(&self) -> String {
        self.bank_provided_id.clone()
    }

    pub const fn amount(&self) -> Usd {
        self.amount
    }

    pub const fn credit_or_debit(&self) -> TransactionDirection {
        self.credit_or_debit
    }

    pub fn metadata(&self) -> String {
        self.metadata.clone()
    }
}

impl From<xand_banks::models::BankTransaction> for BankTransaction {
    fn from(bt: xand_banks::models::BankTransaction) -> Self {
        Self::new(bt.bank_unique_id, bt.amount, bt.txn_type.into(), bt.metadata)
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use xand_banks::xand_money::Money;

    impl BankTransaction {
        pub fn test() -> Self {
            Self::test_with_id("0")
        }

        pub fn test_with_id(bank_provided_id: &str) -> Self {
            Self::new(
                bank_provided_id.to_string(),
                Usd::from_str("100.00").unwrap(),
                TransactionDirection::Credit,
                "metadata".to_string(),
            )
        }

        pub fn test_with_amount_only(amount: &str) -> Self {
            Self::new(
                "123".to_string(),
                Usd::from_str(amount).unwrap(),
                TransactionDirection::Credit,
                "metadata".to_string(),
            )
        }
    }
}
