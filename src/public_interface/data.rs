use crate::domain::data::{DomainBankAccount, DomainHistoryRequest, DomainTransaction};
use chrono::{DateTime, Utc};
use xand_money::Usd;

pub use crate::domain::data::TransactionDirection;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct HistoryRequest {
    bank_account: BankAccount,
    /// Returns transactions where derived timestamp is after given DateTime (non-inclusive)
    history_after: DateTime<Utc>,
}

impl HistoryRequest {
    #[must_use]
    pub const fn new(bank_account: BankAccount, history_after: DateTime<Utc>) -> Self {
        Self {
            bank_account,
            history_after,
        }
    }

    #[must_use]
    pub fn get_bank_account(&self) -> BankAccount {
        self.bank_account.clone()
    }

    #[must_use]
    pub const fn get_history_after(&self) -> DateTime<Utc> {
        self.history_after
    }
}

impl From<HistoryRequest> for DomainHistoryRequest {
    fn from(request: HistoryRequest) -> Self {
        Self::new(request.bank_account.into(), request.history_after)
    }
}
#[derive(Debug, Clone, Hash, Eq, PartialEq)]
pub struct BankAccount(DomainBankAccount);

impl BankAccount {
    #[must_use]
    pub const fn new(routing_number: String, account_number: String) -> Self {
        Self(DomainBankAccount::new(routing_number, account_number))
    }

    #[must_use]
    pub fn get_routing_number(&self) -> String {
        self.0.get_routing_number()
    }

    #[must_use]
    pub fn get_account_number(&self) -> String {
        self.0.get_account_number()
    }
}

impl From<BankAccount> for DomainBankAccount {
    fn from(account: BankAccount) -> Self {
        account.0
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Transaction(DomainTransaction);

impl Transaction {
    #[must_use]
    pub fn new(amount: Usd, credit_or_debit: TransactionDirection, metadata: String) -> Self {
        Self(DomainTransaction::new(amount, credit_or_debit, metadata))
    }

    #[must_use]
    pub fn get_id(&self) -> String {
        self.0.get_transaction_id()
    }

    #[must_use]
    pub const fn get_amount(&self) -> Usd {
        self.0.get_amount()
    }

    #[must_use]
    pub const fn get_credit_or_debit(&self) -> TransactionDirection {
        self.0.get_credit_or_debit()
    }

    #[must_use]
    pub fn get_metadata(&self) -> String {
        self.0.get_metadata()
    }
}

impl From<DomainTransaction> for Transaction {
    fn from(transaction: DomainTransaction) -> Self {
        Self(transaction)
    }
}
