use crate::public_interface::config::{
    error::{Error, ExpectedFile},
    StoreConfiguration,
};
use assert_matches::assert_matches;
use std::{os::unix::ffi::OsStrExt, path::Path};

#[test]
fn make_paths_absolute__sqlite_path_is_unchanged_when_absolute() {
    // Given
    let store_config = StoreConfiguration::Sqlite {
        path: "/absolute/path/to/file.db".to_string(),
    };

    // When
    let result = store_config.make_paths_absolute(Path::new("/unused/path")).unwrap();

    // Then
    assert_eq!(result, store_config);
}

#[test]
fn make_paths_absolute__sqlite_path_is_made_absolute_relative_to_given_path() {
    // Given
    let absolute_path_to_dir = "/absolute";
    let relative_path_to_file = "path/to/file.db";
    let store_config = StoreConfiguration::Sqlite {
        path: relative_path_to_file.to_string(),
    };

    // When
    let result = store_config
        .make_paths_absolute(Path::new(absolute_path_to_dir))
        .unwrap();

    // Then
    let expected_store_config = StoreConfiguration::Sqlite {
        path: format!("{}/{}", absolute_path_to_dir, relative_path_to_file),
    };
    assert_eq!(result, expected_store_config);
}

#[test]
#[cfg(unix)]
fn make_paths_absolute__returns_error_when_given_bad_path() {
    // Given
    let bad_str_absolute_path = Path::new(std::ffi::OsStr::from_bytes(&[0x80]));
    let relative_path_to_file = "path/to/file.db";
    let store_config = StoreConfiguration::Sqlite {
        path: relative_path_to_file.to_string(),
    };

    // When
    let error = store_config
        .make_paths_absolute(Path::new(bad_str_absolute_path))
        .unwrap_err();

    // Then
    let expected_error_path = format!("{}/{}", std::char::REPLACEMENT_CHARACTER, relative_path_to_file);
    assert_matches!(error, Error::InvalidFilePath { expected_file, path }
        if expected_file == ExpectedFile::Sqlite && path == expected_error_path);
}
