use std::time::Duration;

use crate::public_interface::{BankClientConfiguration, BankConfiguration, SynchronizationPolicyConfiguration};

impl BankConfiguration {
    #[must_use]
    pub fn test_many_with(num_banks: usize, num_accounts_per_bank: usize) -> Vec<Self> {
        (0..num_banks)
            .map(|bank| Self::test_with(bank, num_accounts_per_bank))
            .collect()
    }

    #[must_use]
    pub fn test_with(bank_index: usize, num_accounts: usize) -> Self {
        Self {
            account_numbers: (0..num_accounts)
                .map(|account| format!("bank {}, account {}", bank_index, account))
                .collect(),
            bank: BankClientConfiguration::Test1,
            routing_number: format!("bank {}", bank_index),
            sync_policy: SynchronizationPolicyConfiguration {
                cooldown_timeout: Duration::ZERO,
                periodic_synchronization_interval: Duration::MAX,
            },
        }
    }
}
