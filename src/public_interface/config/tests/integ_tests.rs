use std::{path::PathBuf, time::Duration};

use crate::public_interface::{
    config::{
        BankTransactionRepositoryConfiguration, BankTransactionRepositoryConfigurationData, SecretStoreConfiguration,
        StoreConfiguration,
    },
    BankClientConfiguration, BankConfiguration, SynchronizationPolicyConfiguration,
};
use assert_matches::assert_matches;
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;

#[test]
fn can_load_sample_config() {
    // Given
    let config_path = {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("sample_config.yaml");
        path
    };

    // When
    let file = std::fs::File::open(config_path).unwrap();
    let config_data: BankTransactionRepositoryConfigurationData = serde_yaml::from_reader(file).unwrap();
    let config = config_data.load("unused").unwrap();

    // Then
    let expected_secret_store_config = LocalFileSecretStoreConfiguration {
        yaml_file_path: "/secrets.yaml".to_string(),
    };
    let expected_config = BankTransactionRepositoryConfiguration {
        banks: [
            BankConfiguration {
                routing_number: "1111111".to_string(),
                account_numbers: vec!["898989".to_string(), "676767".to_string()].into_iter().collect(),
                bank: BankClientConfiguration::Test2,
                sync_policy: SynchronizationPolicyConfiguration {
                    cooldown_timeout: Duration::from_millis(10000),
                    periodic_synchronization_interval: Duration::from_millis(15000),
                },
            },
            BankConfiguration {
                routing_number: "2222222".to_string(),
                account_numbers: vec!["454545".to_string(), "232323".to_string()].into_iter().collect(),
                bank: BankClientConfiguration::Test1,
                sync_policy: SynchronizationPolicyConfiguration {
                    cooldown_timeout: Duration::from_millis(12000),
                    periodic_synchronization_interval: Duration::from_millis(22000),
                },
            },
        ]
        .iter()
        .cloned()
        .collect(),
        store: StoreConfiguration::Sqlite {
            path: "/absolute/path/to/file.db".to_string(),
        },
        secret_store: SecretStoreConfiguration::LocalFile(expected_secret_store_config.clone()),
    };
    assert_eq!(config.banks, expected_config.banks);
    assert_eq!(config.store, expected_config.store);
    assert_matches!(config.secret_store, SecretStoreConfiguration::LocalFile(config) if config == expected_secret_store_config);
}
