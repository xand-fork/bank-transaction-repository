use insta::assert_debug_snapshot;
use std::time::Duration;
use xand_secrets::{ExposeSecret, Secret};

use crate::public_interface::{
    config::{BankTransactionRepositoryConfigurationData, SecretStoreConfiguration, StoreConfiguration},
    BankClientConfiguration, BankConfiguration, SynchronizationPolicyConfiguration,
};
use assert_matches::assert_matches;
use xand_secrets_local_file::LocalFileSecretStoreConfiguration;
use xand_secrets_vault::VaultConfiguration;

#[test]
fn deserialize__bank_client_config_can_be_deserialized() {
    // Given
    let serialized_bank_client_config = "test1";

    // When
    let deserialized: BankClientConfiguration = serde_yaml::from_str(serialized_bank_client_config).unwrap();

    // Then
    assert_eq!(deserialized, BankClientConfiguration::Test1);
}

#[test]
fn deserialize__sync_policy_config_can_be_deserialized() {
    // Given
    let serialized_sync_policy_config = r#"
        cooldown_timeout_secs: 10
        periodic_synchronization_interval_secs: 15
    "#;

    // When
    let deserialized: SynchronizationPolicyConfiguration = serde_yaml::from_str(serialized_sync_policy_config).unwrap();

    // Then
    let expected_sync_policy_config = SynchronizationPolicyConfiguration {
        cooldown_timeout: Duration::from_millis(10000),
        periodic_synchronization_interval: Duration::from_millis(15000),
    };
    assert_eq!(deserialized, expected_sync_policy_config);
}

#[test]
fn deserialize__sync_policy_config_disallows_negative_durations() {
    // Given
    let serialized_sync_policy_config = r#"
        cooldown_timeout_secs: -1
        periodic_synchronization_interval_secs: 15
    "#;

    // When
    let result: Result<SynchronizationPolicyConfiguration, _> = serde_yaml::from_str(serialized_sync_policy_config);

    // Then
    let error = result.unwrap_err();
    assert_debug_snapshot!(error, @r###"
    Message(
        "invalid value: integer `-1`, expected u64",
        Some(
            Pos {
                marker: Marker {
                    index: 32,
                    line: 2,
                    col: 31,
                },
                path: "cooldown_timeout_secs",
            },
        ),
    )
    "###);
}

#[test]
fn deserialize__sync_policy_config_disallows_fractional_numbers() {
    // Given
    let serialized_sync_policy_config = r#"
        cooldown_timeout_secs: 0.5
        periodic_synchronization_interval_secs: 15
    "#;

    // When
    let result: Result<SynchronizationPolicyConfiguration, _> = serde_yaml::from_str(serialized_sync_policy_config);

    // Then
    let error = result.unwrap_err();
    assert_debug_snapshot!(error, @r###"
    Message(
        "invalid type: floating point `0.5`, expected u64",
        Some(
            Pos {
                marker: Marker {
                    index: 32,
                    line: 2,
                    col: 31,
                },
                path: "cooldown_timeout_secs",
            },
        ),
    )
    "###);
}

#[test]
fn deserialize__bank_configuration_can_be_deserialized() {
    // Given
    let serialized_bank_configuration = r#"
      routing_number: "010101"
      account_numbers: ["898989", "676767"]
      bank: test2
      sync_policy:
        cooldown_timeout_secs: 10
        periodic_synchronization_interval_secs: 15
    "#;

    // When
    let deserialized: BankConfiguration = serde_yaml::from_str(serialized_bank_configuration).unwrap();

    // Then
    let expected_bank_config = BankConfiguration {
        routing_number: "010101".to_string(),
        account_numbers: vec!["898989".to_string(), "676767".to_string()].into_iter().collect(),
        bank: BankClientConfiguration::Test2,
        sync_policy: SynchronizationPolicyConfiguration {
            cooldown_timeout: Duration::from_millis(10000),
            periodic_synchronization_interval: Duration::from_millis(15000),
        },
    };
    assert_eq!(deserialized, expected_bank_config);
}

#[test]
fn deserialize__store_configuration_can_be_deserialized() {
    // Given
    let serialized = r#"
        backend: Sqlite
        path: path/to/file.db
    "#;

    // When
    let deserialized: StoreConfiguration = serde_yaml::from_str(serialized).unwrap();

    // Then
    let expected_config = StoreConfiguration::Sqlite {
        path: "path/to/file.db".to_string(),
    };
    assert_eq!(deserialized, expected_config);
}

#[test]
fn deserialize__local_file_secret_store_configuration_can_be_deserialized() {
    // Given
    let serialized = r#"
        local_file:
            yaml-file-path: path/to/secrets.yaml
    "#;

    // When
    let deserialized: SecretStoreConfiguration = serde_yaml::from_str(serialized).unwrap();

    // Then
    let expected_config = LocalFileSecretStoreConfiguration {
        yaml_file_path: "path/to/secrets.yaml".to_string(),
    };
    assert_matches!(deserialized, SecretStoreConfiguration::LocalFile(config) if config == expected_config);
}

#[test]
fn deserialize__vault_secret_store_configuration_can_be_deserialized() {
    // Given
    let serialized = r#"
        vault:
            http-endpoint: https://example.com/secrets
            token: super-secret-token
    "#;

    // When
    let deserialized: SecretStoreConfiguration = serde_yaml::from_str(serialized).unwrap();

    // Then
    let expected_config = VaultConfiguration {
        http_endpoint: String::from("https://example.com/secrets"),
        token: Secret::new(String::from("super-secret-token")),
        additional_https_root_certificate_files: None,
    };
    assert_matches!(
        deserialized,
        SecretStoreConfiguration::Vault(
            VaultConfiguration { http_endpoint, token, additional_https_root_certificate_files }
        ) if http_endpoint == expected_config.http_endpoint
            && token.expose_secret() == expected_config.token.expose_secret()
            && additional_https_root_certificate_files == expected_config.additional_https_root_certificate_files
    );
}

#[test]
fn deserialize__bank_transaction_repository_configuration_data() {
    // Given
    let serialized_bank_configuration = r#"
        banks:
          - routing_number: "1111111"
            account_numbers: ["898989", "676767"]
            bank: test2
            sync_policy:
              cooldown_timeout_secs: 10
              periodic_synchronization_interval_secs: 15
          - routing_number: "2222222"
            account_numbers: ["454545", "232323"]
            bank: test1
            sync_policy:
              cooldown_timeout_secs: 12
              periodic_synchronization_interval_secs: 22
        store:
          backend: Sqlite
          path: path/to/file.db
        secret_store:
          local_file:
            yaml-file-path: path/to/secrets.yaml
    "#;

    // When
    let deserialized: BankTransactionRepositoryConfigurationData =
        serde_yaml::from_str(serialized_bank_configuration).unwrap();

    // Then
    let expected_secret_store_config = LocalFileSecretStoreConfiguration {
        yaml_file_path: "path/to/secrets.yaml".to_string(),
    };
    let expected_config = BankTransactionRepositoryConfigurationData {
        banks: [
            BankConfiguration {
                routing_number: "1111111".to_string(),
                account_numbers: vec!["898989".to_string(), "676767".to_string()].into_iter().collect(),
                bank: BankClientConfiguration::Test2,
                sync_policy: SynchronizationPolicyConfiguration {
                    cooldown_timeout: Duration::from_millis(10000),
                    periodic_synchronization_interval: Duration::from_millis(15000),
                },
            },
            BankConfiguration {
                routing_number: "2222222".to_string(),
                account_numbers: vec!["454545".to_string(), "232323".to_string()].into_iter().collect(),
                bank: BankClientConfiguration::Test1,
                sync_policy: SynchronizationPolicyConfiguration {
                    cooldown_timeout: Duration::from_millis(12000),
                    periodic_synchronization_interval: Duration::from_millis(22000),
                },
            },
        ]
        .iter()
        .cloned()
        .collect(),
        store: StoreConfiguration::Sqlite {
            path: "path/to/file.db".to_string(),
        },
        secret_store: SecretStoreConfiguration::LocalFile(expected_secret_store_config.clone()),
    };
    assert_eq!(deserialized.banks, expected_config.banks);
    assert_eq!(deserialized.store, expected_config.store);
    assert_matches!(deserialized.secret_store, SecretStoreConfiguration::LocalFile(config) if config == expected_secret_store_config);
}
