use crate::{
    periodic::error::Error as PeriodicError,
    store::backends::sqlite_database::error::Error as SqliteInitializationError,
    transaction_history_provider::{
        error::Error as TransactionHistoryProviderError, history_puller::error::Error as HistoryPullerError,
    },
};
use std::error::Error as StdError;
use thiserror::Error;
use xand_secrets_vault::VaultSecretStoreCreationError;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    SqliteDatabaseInitializationError(#[from] SqliteInitializationError),
    #[error(transparent)]
    SecretStoreCreationError(Box<dyn StdError + Send + Sync + 'static>),
    #[error(transparent)]
    HistoryProviderCreationError(#[from] TransactionHistoryProviderError),
    #[error(transparent)]
    PeriodicError(#[from] PeriodicError),
    #[error(transparent)]
    HistoryPullerError(#[from] HistoryPullerError),
}

impl From<VaultSecretStoreCreationError> for Error {
    fn from(error: VaultSecretStoreCreationError) -> Self {
        Self::SecretStoreCreationError(Box::new(error))
    }
}
