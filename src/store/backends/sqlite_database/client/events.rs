use crate::store::backends::sqlite_database::db::models::transaction::new_transaction::NewTransaction;
use std::fmt::Debug;

#[derive(Debug)]
pub struct FailedToInsert<Error: Debug> {
    pub transaction: NewTransaction,
    pub error: Error,
}
