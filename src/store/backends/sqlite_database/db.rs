use crate::store::backends::sqlite_database::{
    client::SqliteDatabaseClient,
    db::{
        events::{DatabaseInitialTransactionRecord, DatabaseMigrationCompleted},
        schema::transaction_record::{columns::transaction_hash_id, dsl::transaction_record},
    },
};
use diesel::{dsl::count, QueryDsl, RunQueryDsl};
use error::{
    database::Error as DatabaseError, Error as SqliteInitializationError, Result as SqliteInitializationResult,
};
use std::path::Path;

mod connection_pool_builder;
pub mod error;
pub mod models;
pub mod schema;
pub mod sqlite_connection_pool;

#[cfg(test)]
const SQLITE_IN_MEMORY_PATH: &str = ":memory:";

embed_migrations!();

pub enum Persistence {
    #[cfg(test)]
    InMemory,
    File(String),
}

impl Persistence {
    // There is only a single variant when building in non-test mode.
    #[allow(clippy::infallible_destructuring_match)]
    fn db_path(&self) -> &str {
        match self {
            #[cfg(test)]
            Self::InMemory => SQLITE_IN_MEMORY_PATH,
            Self::File(path) => path,
        }
    }
}

pub struct SqliteDatabase(Persistence);

impl SqliteDatabase {
    #[must_use]
    pub fn open_with_file(path: &str) -> Self {
        Self(Persistence::File(path.into()))
    }

    #[cfg(test)]
    #[must_use]
    pub const fn new_in_memory() -> Self {
        Self(Persistence::InMemory)
    }

    pub fn init(&self) -> SqliteInitializationResult<SqliteDatabaseClient> {
        let db_path = self.0.db_path();

        // Create directories necessary to create db file if they don't already exist
        if let Some(parent) = Path::new(db_path).parent() {
            if !parent.exists() {
                std::fs::create_dir_all(parent)
                    .map_err(|_| SqliteInitializationError::InvalidSqliteFilePath(db_path.to_string()))?;
            }
        }

        let connection_pool = connection_pool_builder::build_connection_pool(db_path)?;

        connection_pool.connect_and_run(|connection| {
            let mut output: Vec<u8> = vec![];
            embedded_migrations::run_with_output(connection, &mut output)
                .map_err(|err| DatabaseError::MigrationError(Box::new(err)))?;
            let output_str = std::str::from_utf8(&output).unwrap_or("error in converting migration output to str");
            tracing::info!(message = ?DatabaseMigrationCompleted{output: output_str});

            let count = transaction_record
                .select(count(transaction_hash_id))
                .first(connection)
                .map_err(|err| DatabaseError::FetchError(Box::new(err)))?;
            tracing::info!(message = ?DatabaseInitialTransactionRecord{count});
            Ok(())
        })?;
        Ok(SqliteDatabaseClient::new(connection_pool))
    }
}

mod events {
    #[derive(Debug)]
    pub struct DatabaseMigrationCompleted<'a> {
        pub output: &'a str,
    }

    #[derive(Debug)]
    pub struct DatabaseInitialTransactionRecord {
        pub count: i64,
    }
}

#[cfg(test)]
mod tests {
    use crate::store::backends::sqlite_database::{
        db::events::{DatabaseInitialTransactionRecord, DatabaseMigrationCompleted},
        SqliteDatabase,
    };
    use std::path::Path;
    use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
    use tracing_assert_macros::tracing_capture_event_fields;

    #[test]
    fn init__can_init_in_memory_test_db() {
        // Given
        let db = SqliteDatabase::new_in_memory();

        // When
        db.init().unwrap();

        // Then
    }

    #[test]
    fn init__can_init_file_based_test_db() {
        // Given
        let file = "test_db/test_db.sqlite3";
        let db = SqliteDatabase::open_with_file(file);

        // When
        db.init().unwrap();
        let file_path = Path::new(file);

        // Then
        assert!(file_path.exists());
        assert_ne!(file_path.metadata().unwrap().len(), 0);
    }

    #[test]
    fn init__does_not_overwrite_existing_directories() {
        // Given a directory with a dummy file in it
        let dir = "test_db";
        let dummy_file = format!("{}/dummy.txt", dir);
        let dummy_file_path = Path::new(dummy_file.as_str());

        let parent = dummy_file_path.parent().unwrap();
        std::fs::create_dir_all(parent).unwrap();
        std::fs::File::create(dummy_file_path).unwrap();

        // And a database within the same directory
        let db_file = format!("{}/another_test_db.sqlite.sqlite3", dir);
        let db = SqliteDatabase::open_with_file(db_file.as_str());
        let db_file_path = Path::new(db_file.as_str());

        // When
        db.init().unwrap();

        // Then the dummy file still exists meaning the directory wasn't overwritten
        assert!(dummy_file_path.exists());
        assert!(db_file_path.exists());
        assert_ne!(db_file_path.metadata().unwrap().len(), 0);
    }

    #[test]
    fn init__emits_migration_output() {
        // Given
        let db = SqliteDatabase::new_in_memory();

        // When
        let events = tracing_capture_event_fields!({ db.init().unwrap() });

        // Then
        let expected_event: Vec<(String, String)> = vec![(
            "message".into(),
            DatabaseMigrationCompleted {
                output: "Running migration 20210817182532\n",
            }
            .debug_fmt(),
        )];
        assert!(events.contains(&expected_event), "{:?}", events);
    }

    #[test]
    fn init__emits_transaction_count() {
        // Given
        let db = SqliteDatabase::new_in_memory();

        // When
        let events = tracing_capture_event_fields!({ db.init().unwrap() });

        // Then
        let expected_event: Vec<(String, String)> = vec![(
            "message".into(),
            DatabaseInitialTransactionRecord { count: 0 }.debug_fmt(),
        )];
        assert!(events.contains(&expected_event), "{:?}", events);
    }
}
