pub mod database;

use crate::store::client::transaction_store::{
    insert::error::Error as InsertClientError, query::error::Error as QueryClientError,
};
use database::Error as DatabaseError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error("Error building connection pool: {}", .0)]
    BuildConnectionPool(String),
    #[error(transparent)]
    DatabaseError(#[from] DatabaseError),
    #[error("Invalid sqlite file path: {}", .0)]
    InvalidSqliteFilePath(String),
}

impl From<Error> for InsertClientError {
    fn from(err: Error) -> Self {
        Self::BackendError(Box::new(err))
    }
}

impl From<Error> for QueryClientError {
    fn from(err: Error) -> Self {
        Self::BackendError(Box::new(err))
    }
}
