use std::error::Error as StdError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    ConnectionError(Box<dyn StdError + Send + Sync + 'static>),
    #[error(transparent)]
    MigrationError(Box<dyn StdError + Send + Sync + 'static>),
    #[error(transparent)]
    InsertError(Box<dyn StdError + Send + Sync + 'static>),
    #[error(transparent)]
    FetchError(Box<dyn StdError + Send + Sync + 'static>),
}
