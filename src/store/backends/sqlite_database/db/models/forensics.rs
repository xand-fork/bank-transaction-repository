use crate::store::backends::sqlite_database::db::{
    models::ingestion::date_time_value::DateTimeValue, schema::forensics,
};
use diesel::Queryable;

#[derive(Debug, Clone, Insertable, PartialEq, Eq)]
#[table_name = "forensics"]
pub struct NewForensics {
    bank_provided_id: String,
    transaction_hash_id: String,
    timestamp: DateTimeValue,
}

impl NewForensics {
    pub const fn new(bank_provided_id: String, transaction_hash_id: String, timestamp: DateTimeValue) -> Self {
        Self {
            bank_provided_id,
            transaction_hash_id,
            timestamp,
        }
    }
}

#[allow(dead_code)]
#[derive(Queryable)]
pub struct StoredForensics {
    id: i32,
    bank_provided_id: String,
    transaction_hash_id: String,
    timestamp: DateTimeValue,
}
