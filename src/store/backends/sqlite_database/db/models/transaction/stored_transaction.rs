use crate::{
    domain::data::DomainTransaction,
    store::backends::sqlite_database::db::models::transaction::{
        monetary_value::MonetaryValue, transaction_direction_value::TransactionDirectionValue,
    },
};
use diesel::Queryable;

#[derive(Queryable)]
pub struct StoredTransaction {
    pub id: i32,
    pub account_id: i32,
    pub ingestion_id: i32,
    pub transaction_hash_id: String,
    pub amount: MonetaryValue,
    pub credit_or_debit: TransactionDirectionValue,
    pub metadata: String,
}

impl From<StoredTransaction> for DomainTransaction {
    fn from(txn: StoredTransaction) -> Self {
        Self::new(txn.amount.val(), txn.credit_or_debit.val(), txn.metadata)
    }
}

impl From<&StoredTransaction> for DomainTransaction {
    fn from(txn: &StoredTransaction) -> Self {
        Self::new(txn.amount.val(), txn.credit_or_debit.val(), txn.metadata.clone())
    }
}
