use diesel::{
    backend::Backend,
    deserialize,
    deserialize::FromSql,
    serialize,
    serialize::{Output, ToSql},
    sqlite::Sqlite,
};
use std::io::Write;
use xand_money::{Money, Usd};

#[derive(Debug, Clone, PartialEq, Eq, AsExpression, FromSqlRow)]
#[sql_type = "diesel::sql_types::Text"]
pub struct MonetaryValue {
    val: Usd,
}

impl MonetaryValue {
    pub const fn new(val: Usd) -> Self {
        Self { val }
    }

    pub const fn val(&self) -> Usd {
        self.val
    }
}

impl FromSql<diesel::sql_types::Text, Sqlite> for MonetaryValue {
    fn from_sql(bytes: Option<&<Sqlite as Backend>::RawValue>) -> deserialize::Result<Self> {
        let string_representation = <String as FromSql<diesel::sql_types::Text, Sqlite>>::from_sql(bytes)?;
        let usd_representation = Usd::from_str(&string_representation)?;
        Ok(Self::new(usd_representation))
    }
}

impl ToSql<diesel::sql_types::Text, Sqlite> for MonetaryValue {
    fn to_sql<W: Write>(&self, out: &mut Output<'_, W, Sqlite>) -> serialize::Result {
        let val = self.val.into_major_units().to_string();
        <String as ToSql<diesel::sql_types::Text, Sqlite>>::to_sql(&val, out)
    }
}
