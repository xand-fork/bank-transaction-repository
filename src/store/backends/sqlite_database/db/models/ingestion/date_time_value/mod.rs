pub mod error;

use chrono::{DateTime, NaiveDateTime, Utc};
use diesel::{
    backend::Backend,
    deserialize,
    deserialize::FromSql,
    serialize,
    serialize::{Output, ToSql},
    sqlite::Sqlite,
};
use std::io::Write;

#[derive(Debug, Clone, PartialEq, Eq, AsExpression, FromSqlRow)]
#[sql_type = "diesel::sql_types::BigInt"]
pub struct DateTimeValue {
    val: DateTime<Utc>,
}

impl DateTimeValue {
    pub const fn new(val: DateTime<Utc>) -> Self {
        Self { val }
    }

    pub const fn val(&self) -> DateTime<Utc> {
        self.val
    }
}

impl FromSql<diesel::sql_types::BigInt, Sqlite> for DateTimeValue {
    fn from_sql(bytes: Option<&<Sqlite as Backend>::RawValue>) -> deserialize::Result<Self> {
        let integer_representation = <i64 as FromSql<diesel::sql_types::BigInt, Sqlite>>::from_sql(bytes)?;
        let native_datetime = NaiveDateTime::from_timestamp_opt(integer_representation, 0)
            .ok_or_else(|| error::Error::IntegerParse(integer_representation.to_string()))?;
        let datetime_utc: DateTime<Utc> = DateTime::from_utc(native_datetime, Utc);
        Ok(Self::new(datetime_utc))
    }
}

impl ToSql<diesel::sql_types::BigInt, Sqlite> for DateTimeValue {
    fn to_sql<W: Write>(&self, out: &mut Output<'_, W, Sqlite>) -> serialize::Result {
        let val = self.val.timestamp();
        <i64 as ToSql<diesel::sql_types::BigInt, Sqlite>>::to_sql(&val, out)
    }
}
