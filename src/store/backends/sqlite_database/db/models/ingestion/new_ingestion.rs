use crate::store::backends::sqlite_database::db::{
    models::ingestion::date_time_value::DateTimeValue, schema::ingestion,
};

#[derive(Debug, Clone, Insertable)]
#[table_name = "ingestion"]
pub struct NewIngestion {
    pub account_id: i32,
    pub timestamp: DateTimeValue,
}

impl NewIngestion {
    pub const fn new(account_id: i32, timestamp: DateTimeValue) -> Self {
        Self { account_id, timestamp }
    }
}
