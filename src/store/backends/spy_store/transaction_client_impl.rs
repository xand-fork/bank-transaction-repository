use crate::{
    domain::data::{DomainBankAccount, DomainTransaction, IngestionRecord, TimestampedTransaction},
    store::{
        backends::spy_store::SpyStore,
        client::transaction_store::{
            date_range::{DateRange, DateRangePolicy},
            insert::{
                error::{Error as InsertError, Result as InsertResult},
                TransactionStoreInsertClient,
            },
            query::{
                error::{Error as QueryError, Result as QueryResult},
                TransactionStoreQueryClient,
            },
        },
    },
};
use chrono::{DateTime, Utc};
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};
use thiserror::Error;

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct InsertCallRecord {
    pub ingestion_record: IngestionRecord,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct ContainsCallRecord {
    pub bank_account: DomainBankAccount,
    pub domain_transaction: DomainTransaction,
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub struct QueryCallRecord {
    pub bank_account: DomainBankAccount,
    pub date_range: DateRange,
    pub date_range_policy: DateRangePolicy,
}

#[derive(Debug, Error)]
pub enum TestError {
    #[error("Test Error")]
    Test,
}

#[derive(Clone, Debug)]
pub struct SpyStoreTransactionClientCalls {
    stored_insert_result: Arc<Mutex<InsertResult<()>>>,
    insert_calls: Arc<Mutex<Vec<InsertCallRecord>>>,
    stored_query_result: Arc<Mutex<QueryResult<Vec<DomainTransaction>>>>,
    query_calls: Arc<Mutex<Vec<QueryCallRecord>>>,
}

impl SpyStoreTransactionClientCalls {
    pub fn get_insert_calls(&self) -> Vec<InsertCallRecord> {
        self.insert_calls.lock().unwrap().to_owned()
    }

    pub fn set_insert_result(&self, stored_results: InsertResult<()>) {
        *self.stored_insert_result.lock().unwrap() = stored_results;
    }

    pub fn get_query_calls(&self) -> Vec<QueryCallRecord> {
        self.query_calls.lock().unwrap().to_owned()
    }

    pub fn set_query_result(&self, stored_results: QueryResult<Vec<DomainTransaction>>) {
        *self.stored_query_result.lock().unwrap() = stored_results;
    }
}

impl Default for SpyStoreTransactionClientCalls {
    fn default() -> Self {
        Self {
            stored_insert_result: Arc::new(Mutex::new(Ok(()))),
            insert_calls: Arc::default(),
            stored_query_result: Arc::new(Mutex::new(Ok(vec![]))),
            query_calls: Arc::default(),
        }
    }
}

#[async_trait::async_trait]
impl TransactionStoreInsertClient for SpyStore {
    async fn insert(&self, ingestion_record: IngestionRecord) -> InsertResult<()> {
        self.transaction_client_calls
            .insert_calls
            .lock()
            .unwrap()
            .push(InsertCallRecord { ingestion_record });

        match self
            .transaction_client_calls
            .stored_insert_result
            .lock()
            .unwrap()
            .as_ref()
        {
            Ok(_) => Ok(()),
            Err(_) => Err(InsertError::BackendError(Box::new(TestError::Test))),
        }
    }
}

#[async_trait::async_trait]
impl TransactionStoreQueryClient for SpyStore {
    async fn contains_transaction_with_id(
        &self,
        _bank_account: &DomainBankAccount,
        _transaction_id: &str,
    ) -> QueryResult<bool> {
        unimplemented!();
    }

    async fn get_last_ingest_time(&self, _bank_account: &DomainBankAccount) -> QueryResult<Option<DateTime<Utc>>> {
        unimplemented!();
    }

    async fn get_timestamped_txns_by_id(
        &self,
        _bank_account: &DomainBankAccount,
        _txn_ids: Vec<&str>,
    ) -> QueryResult<HashMap<String, TimestampedTransaction>> {
        Ok(HashMap::new()) // TODO - assess if this needs to be implemented: https://dev.azure.com/transparentsystems/xand-network/_workitems/edit/7191
    }

    async fn query(
        &self,
        bank_account: &DomainBankAccount,
        date_range: &DateRange,
        date_range_policy: DateRangePolicy,
    ) -> QueryResult<Vec<DomainTransaction>> {
        self.transaction_client_calls
            .query_calls
            .lock()
            .unwrap()
            .push(QueryCallRecord {
                bank_account: bank_account.clone(),
                date_range: date_range.clone(),
                date_range_policy,
            });

        match self
            .transaction_client_calls
            .stored_query_result
            .lock()
            .unwrap()
            .as_ref()
        {
            Ok(txns) => Ok(txns.clone()),
            Err(_) => Err(QueryError::BackendError(Box::new(TestError::Test))),
        }
    }
}
