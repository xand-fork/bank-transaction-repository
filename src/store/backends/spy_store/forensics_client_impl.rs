use crate::store::{backends::spy_store::SpyStore, client::forensics_store::ForensicsStoreClient};
use async_trait::async_trait;
use chrono::{DateTime, Utc};

#[async_trait]
impl ForensicsStoreClient for SpyStore {
    async fn insert_if_new(
        &self,
        _bank_txn_id: String,
        _synthetic_id: String,
        _timestamp: &DateTime<Utc>,
    ) -> crate::store::client::forensics_store::error::Result<bool> {
        unimplemented!()
    }
}
