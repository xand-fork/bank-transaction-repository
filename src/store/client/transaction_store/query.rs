use super::{
    date_range::{DateRange, DateRangePolicy},
    TransactionStoreClient,
};
use crate::domain::data::{DomainBankAccount, DomainTransaction, TimestampedTransaction};
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use error::Result;
use std::{collections::HashMap, sync::Arc};

pub mod error;

#[async_trait]
pub trait TransactionStoreQueryClient: Send + Sync {
    async fn contains_transaction_with_id(
        &self,
        bank_account: &DomainBankAccount,
        transaction_id: &str,
    ) -> Result<bool>;
    async fn get_last_ingest_time(&self, bank_account: &DomainBankAccount) -> Result<Option<DateTime<Utc>>>;
    ///
    /// For any entry in `txn_ids` which does not have a corresponding entry in the store,
    /// the returned `HashMap` omits the record and returns those that exist.
    async fn get_timestamped_txns_by_id(
        &self,
        bank_account: &DomainBankAccount,
        txn_ids: Vec<&str>,
    ) -> Result<HashMap<String, TimestampedTransaction>>;
    async fn query(
        &self,
        bank_account: &DomainBankAccount,
        date_range: &DateRange,
        date_range_policy: DateRangePolicy,
    ) -> Result<Vec<DomainTransaction>>;
}

#[async_trait]
impl TransactionStoreQueryClient for Arc<dyn TransactionStoreClient> {
    async fn contains_transaction_with_id(
        &self,
        bank_account: &DomainBankAccount,
        transaction_id: &str,
    ) -> Result<bool> {
        self.as_ref()
            .contains_transaction_with_id(bank_account, transaction_id)
            .await
    }

    async fn get_last_ingest_time(&self, bank_account: &DomainBankAccount) -> Result<Option<DateTime<Utc>>> {
        self.as_ref().get_last_ingest_time(bank_account).await
    }

    async fn get_timestamped_txns_by_id(
        &self,
        bank_account: &DomainBankAccount,
        txn_ids: Vec<&str>,
    ) -> Result<HashMap<String, TimestampedTransaction>> {
        self.as_ref().get_timestamped_txns_by_id(bank_account, txn_ids).await
    }

    async fn query(
        &self,
        bank_account: &DomainBankAccount,
        date_range: &DateRange,
        date_range_policy: DateRangePolicy,
    ) -> Result<Vec<DomainTransaction>> {
        self.as_ref().query(bank_account, date_range, date_range_policy).await
    }
}

#[async_trait::async_trait]
impl<T: TransactionStoreClient> TransactionStoreQueryClient for Arc<T> {
    async fn contains_transaction_with_id(
        &self,
        bank_account: &DomainBankAccount,
        transaction_id: &str,
    ) -> Result<bool> {
        self.as_ref()
            .contains_transaction_with_id(bank_account, transaction_id)
            .await
    }

    async fn get_last_ingest_time(
        &self,
        bank_account: &DomainBankAccount,
    ) -> Result<Option<chrono::DateTime<chrono::Utc>>> {
        self.as_ref().get_last_ingest_time(bank_account).await
    }

    async fn get_timestamped_txns_by_id(
        &self,
        bank_account: &DomainBankAccount,
        txn_ids: Vec<&str>,
    ) -> Result<HashMap<String, TimestampedTransaction>> {
        self.as_ref().get_timestamped_txns_by_id(bank_account, txn_ids).await
    }

    async fn query(
        &self,
        bank_account: &DomainBankAccount,
        date_range: &DateRange,
        date_range_policy: DateRangePolicy,
    ) -> Result<Vec<DomainTransaction>> {
        self.as_ref().query(bank_account, date_range, date_range_policy).await
    }
}
