use std::error::Error as StdError;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[allow(clippy::enum_variant_names)]
#[derive(Debug, Error)]
pub enum Error {
    #[error(transparent)]
    BackendError(Box<dyn StdError + Send + Sync + 'static>),
}
