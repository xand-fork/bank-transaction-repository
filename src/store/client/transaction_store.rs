use insert::TransactionStoreInsertClient;
use query::TransactionStoreQueryClient;
use std::fmt::Debug;

pub mod date_range;
pub mod error;
pub mod insert;
pub mod query;

pub trait TransactionStoreClient: TransactionStoreInsertClient + TransactionStoreQueryClient + Debug {}

impl<T: TransactionStoreInsertClient + TransactionStoreQueryClient + Debug> TransactionStoreClient for T {}
