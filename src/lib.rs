#![warn(
    clippy::all,
    // clippy::nursery,
    clippy::pedantic,
    clippy::unreadable_literal,
    rust_2018_idioms
)]
// Safety-critical application lints
#![deny(clippy::pedantic, clippy::float_cmp_const, clippy::unwrap_used)]
#![allow(
    clippy::implicit_return,
    clippy::iter_nth_zero,
    clippy::match_bool,
    clippy::missing_errors_doc,
    clippy::non_ascii_literal,
    clippy::wildcard_imports,
    clippy::module_name_repetitions,
    clippy::no_effect_underscore_binding, // False positives from async_trait
    incomplete_features
)]
// To use the `unsafe` keyword, do not remove the `unsafe_code` attribute entirely.
// Instead, change it to `#![allow(unsafe_code)]` or preferably `#![deny(unsafe_code)]` + opt-in
// with local `#[allow(unsafe_code)]`'s on a case-by-case basis, if practical.
#![forbid(unsafe_code)]
#![forbid(bare_trait_objects)]
// Uncomment before ship to reconcile use of possibly redundant crates, debug remnants, missing
// license files and more
// #![allow(clippy::blanket_clippy_restriction_lints)]
// #![warn(clippy::cargo, clippy::restriction, missing_docs, warnings)]
// #![allow(clippy::implicit_return)]
// App Specific
#![deny(clippy::future_not_send)]
#![cfg_attr(test, allow(non_snake_case, clippy::unwrap_used))]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

pub use public_interface::{
    construct_store_client, error, BankAccount, BankClientConfiguration, BankConfiguration, BankTransactionRepository,
    BankTransactionRepositoryConfiguration, BankTransactionRepositoryConfigurationData, HistoryRequest,
    SecretStoreConfiguration, StoreConfiguration, SynchronizationPolicyConfiguration, Transaction,
    TransactionDirection,
};

#[cfg(feature = "prod-integ")]
pub use store::backends::sqlite_database::SqliteDatabase;

pub use store::backends::sqlite_database::SqliteDatabaseClient;

pub(crate) mod domain;
pub(crate) mod store;

mod ingestor;
mod periodic;
mod public_interface;
mod scheduler;
mod synchronizer;
mod transaction_history_provider;
