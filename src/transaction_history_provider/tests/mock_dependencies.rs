use crate::{
    domain::data::DomainBankAccount,
    ingestor::{bank_client::BankClient, MockIngestor},
    public_interface::{BankClientConfiguration, SynchronizationPolicyConfiguration},
    store::backends::spy_store::SpyStore,
    transaction_history_provider::{
        dependencies::error::Result, tests::SpySynchronizer, TransactionHistoryProviderDependencies,
    },
};
use std::{
    cell::RefCell,
    collections::{HashMap, HashSet},
    sync::Arc,
};

#[derive(Debug, Clone)]
pub struct SynchronizerCreationParams {
    pub routing_number: String,
    pub accounts: HashSet<DomainBankAccount>,
    pub sync_policy: SynchronizationPolicyConfiguration,
}

#[derive(Default)]
pub struct MockTransactionHistoryProviderDependencies {
    ingestor_creations: RefCell<Vec<Box<dyn BankClient>>>,
    synchronizer_creations: RefCell<Vec<SynchronizerCreationParams>>,
    synchronizers: RefCell<HashMap<String, SpySynchronizer>>,
}

impl MockTransactionHistoryProviderDependencies {
    pub fn get_num_ingestor_creations(&self) -> usize {
        self.ingestor_creations.borrow().len()
    }

    pub fn get_ingestor_client_configs(&self) -> Vec<BankClientConfiguration> {
        self.ingestor_creations
            .borrow()
            .iter()
            .map(BankClient::get_config)
            .collect()
    }

    pub fn get_synchronizer_creations(&self) -> Vec<SynchronizerCreationParams> {
        self.synchronizer_creations.borrow().to_owned()
    }

    pub fn get_num_synchronizer_creations(&self) -> usize {
        self.synchronizer_creations.borrow().len()
    }

    pub fn get_synchronizer(&self, routing_number: &str) -> Option<SpySynchronizer> {
        self.synchronizers.borrow().get(routing_number).map(Clone::clone)
    }
}

impl TransactionHistoryProviderDependencies<Arc<SpyStore>, Arc<SpyStore>>
    for MockTransactionHistoryProviderDependencies
{
    type Ingestor = MockIngestor;
    type Synchronizer = SpySynchronizer;

    fn create_ingestor(
        &self,
        bank_client: Box<dyn crate::ingestor::bank_client::BankClient>,
        _store_client: Arc<SpyStore>,
        _forensics_client: Arc<SpyStore>,
    ) -> Self::Ingestor {
        self.ingestor_creations.borrow_mut().push(bank_client);
        MockIngestor::new()
    }

    fn create_synchronizer(
        &self,
        sync_policy: &SynchronizationPolicyConfiguration,
        _ingestor: Self::Ingestor,
        routing_number: &str,
        accounts: HashSet<DomainBankAccount>,
    ) -> Result<Self::Synchronizer> {
        self.synchronizer_creations
            .borrow_mut()
            .push(SynchronizerCreationParams {
                accounts,
                routing_number: String::from(routing_number),
                sync_policy: sync_policy.clone(),
            });

        let synchronizer = Self::Synchronizer::new();
        self.synchronizers
            .borrow_mut()
            .insert(routing_number.to_string(), synchronizer.clone());
        Ok(synchronizer)
    }
}
