use crate::{
    domain::data::DomainBankAccount,
    periodic::{error::Result as PeriodicResult, Periodic, PeriodicHandleImpl},
    synchronizer::synchronizer_trait::{
        error::{Error, Result},
        Synchronizer,
    },
};
use std::sync::{Arc, Mutex};
use thiserror::Error;

#[derive(Clone)]
pub struct SpySynchronizer {
    synchronize_calls: Arc<Mutex<Vec<DomainBankAccount>>>,
    synchronize_result: Arc<Mutex<Result<()>>>,
}

impl SpySynchronizer {
    pub fn new() -> Self {
        Self::default()
    }

    pub fn get_synchronize_calls(&self) -> Vec<DomainBankAccount> {
        self.synchronize_calls.lock().unwrap().to_owned()
    }

    pub fn set_synchronize_result(&self, result: Result<()>) {
        *self.synchronize_result.lock().unwrap() = result;
    }
}

impl Default for SpySynchronizer {
    fn default() -> Self {
        Self {
            synchronize_calls: Arc::default(),
            synchronize_result: Arc::new(Mutex::new(Ok(()))),
        }
    }
}

#[async_trait::async_trait]
impl Synchronizer for SpySynchronizer {
    async fn synchronize(&self, account: &DomainBankAccount) -> Result<()> {
        self.synchronize_calls.lock().unwrap().push(account.clone());

        match self.synchronize_result.lock().unwrap().as_ref() {
            Ok(_) => Ok(()),
            Err(_) => Err(Error::SynchronizeError(Box::new(TestError::Test))),
        }
    }
}

impl Periodic for SpySynchronizer {
    type Handle = PeriodicHandleImpl<()>;

    fn start_periodic_synchronization(&self) -> PeriodicResult<Self::Handle> {
        Ok(PeriodicHandleImpl::new(vec![]))
    }
}

#[derive(Debug, Error)]
pub enum TestError {
    #[error("Test Error")]
    Test,
}
