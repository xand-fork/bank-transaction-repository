pub mod error;

use crate::{
    domain::data::DomainBankAccount,
    ingestor::{bank_client::BankClient, Ingestor, UniqueTransactionIngestor, WallClock},
    public_interface::SynchronizationPolicyConfiguration,
    scheduler::{
        rate_limited_periodic_scheduler::RateLimitedPeriodicScheduler,
        rate_limited_periodic_scheduler_factory::RateLimitedPeriodicSchedulerFactory,
    },
    store::{client::forensics_store::ForensicsStoreClient, TransactionStoreClient},
    synchronizer::{bank_transaction_synchronizer::BankTransactionSynchronizer, synchronizer_trait::Synchronizer},
};
use error::Result;
use std::collections::HashSet;

/// Dependency injection interface for the concrete types used by the `TransactionHistoryProvider`.
///
/// To ease testability, the provider calls into a separate "dependencies" object to construct
/// the backing synchronizer, ingestor and store. Note that backing types which are selectable at
/// runtime are _not_ included in this interface -- they are constructed dynamically.

pub trait TransactionHistoryProviderDependencies<StoreClient: TransactionStoreClient, Forensics: ForensicsStoreClient> {
    type Ingestor: Ingestor;
    type Synchronizer: Synchronizer + Send + Sync;

    fn create_ingestor(
        &self,
        bank_client: Box<dyn BankClient>,
        store_client: StoreClient,
        forensics_client: Forensics,
    ) -> Self::Ingestor;

    fn create_synchronizer(
        &self,
        sync_policy: &SynchronizationPolicyConfiguration,
        ingestor: Self::Ingestor,
        routing_number: &str,
        accounts: HashSet<DomainBankAccount>,
    ) -> Result<Self::Synchronizer>;
}

#[derive(Default)]
pub struct ConcreteTransactionHistoryProviderDependencies;

type ConcreteSynchronizer = BankTransactionSynchronizer<RateLimitedPeriodicScheduler>;

impl<StoreClient: TransactionStoreClient + 'static, Forensics: ForensicsStoreClient + 'static>
    TransactionHistoryProviderDependencies<StoreClient, Forensics> for ConcreteTransactionHistoryProviderDependencies
{
    type Ingestor = UniqueTransactionIngestor<Box<dyn BankClient>, StoreClient, Forensics, WallClock>;
    type Synchronizer = ConcreteSynchronizer;

    fn create_ingestor(
        &self,
        bank_client: Box<dyn BankClient>,
        store_client: StoreClient,
        forensics_client: Forensics,
    ) -> Self::Ingestor {
        UniqueTransactionIngestor::new(bank_client, store_client, forensics_client, WallClock::new())
    }

    fn create_synchronizer(
        &self,
        sync_policy: &SynchronizationPolicyConfiguration,
        ingestor: Self::Ingestor,
        _routing_number: &str,
        accounts: HashSet<DomainBankAccount>,
    ) -> Result<Self::Synchronizer> {
        let scheduler_factory = RateLimitedPeriodicSchedulerFactory::new(
            sync_policy.cooldown_timeout,
            sync_policy.periodic_synchronization_interval,
        );

        ConcreteSynchronizer::new(ingestor, &scheduler_factory, &accounts).map_err(Into::into)
    }
}
