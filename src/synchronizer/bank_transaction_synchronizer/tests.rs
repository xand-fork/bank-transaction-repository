mod test_utils;

use crate::{
    domain::data::DomainBankAccount,
    synchronizer::{
        bank_transaction_synchronizer::{
            error::Error as BankTransactionSynchronizerError, BankTransactionSynchronizer,
        },
        synchronizer_trait::{error::Error as SynchronizerError, Synchronizer},
    },
};
use assert_matches::assert_matches;
use itertools::Itertools;
use test_utils::{
    DummyIngestor, ErroringScheduler, ErroringSchedulerFactory, IdentifiableIngestor, SpyScheduler,
    SpySchedulerFactory, UniformReturnSchedulerFactory,
};

#[test]
fn new__does_not_construct_scheduler_if_given_no_accounts() {
    // Given an empty account list
    let empty_account_list = vec![];

    let factory_spy = SpySchedulerFactory::new();

    // When new is called
    let _synchronizer =
        BankTransactionSynchronizer::new(DummyIngestor, &factory_spy, &empty_account_list.into_iter().collect())
            .unwrap();

    // Then the factory should not be called
    assert!(!factory_spy.was_called());
}

#[test]
fn new__constructs_schedulers_for_each_account() {
    // Given an account list
    let mut account_list = vec![
        DomainBankAccount::from_str("A", "1"),
        DomainBankAccount::from_str("B", "2"),
    ];
    account_list.sort();

    let factory_spy = SpySchedulerFactory::new();

    // When new is called
    let _synchronizer =
        BankTransactionSynchronizer::new(DummyIngestor, &factory_spy, &account_list.clone().into_iter().collect())
            .unwrap();

    // Then the factory should contain all accounts
    let mut actual_account_list = factory_spy.get_accounts();
    actual_account_list.sort();

    assert_eq!(actual_account_list, account_list);
}

#[test]
fn new__errors_when_factory_create_errors() {
    // Given an account list
    let account_list = vec![DomainBankAccount::from_str("A", "1")];

    let erroring_factory = ErroringSchedulerFactory;

    // When new is called
    let result =
        BankTransactionSynchronizer::new(DummyIngestor, &erroring_factory, &account_list.into_iter().collect())
            .unwrap_err();

    // Then the factory should error
    assert_matches!(result, BankTransactionSynchronizerError::SchedulerCreationError(..));
}

#[test]
fn new__provides_shared_reference_to_same_ingestor() {
    const ARBITRARY_IDENTITY_TEST_VALUE: u32 = 42;

    // Given an account list
    let account_list = vec![
        DomainBankAccount::from_str("A", "1"),
        DomainBankAccount::from_str("B", "2"),
    ];

    let factory_spy = SpySchedulerFactory::new();

    // When new is called
    let _synchronizer = BankTransactionSynchronizer::new(
        IdentifiableIngestor::new(),
        &factory_spy,
        &account_list.into_iter().collect(),
    )
    .unwrap();

    // Then a change applied to one reference is perceived by the other
    let (ref_a, ref_b) = factory_spy.get_ingestors().into_iter().collect_tuple().unwrap();
    ref_a.test_set_identity(ARBITRARY_IDENTITY_TEST_VALUE);
    assert_eq!(ref_b.test_get_identity(), ARBITRARY_IDENTITY_TEST_VALUE);
}

#[tokio::test]
async fn synchronize__returns_error_when_given_unknown_account() {
    // Given a requested account
    let requested_account = DomainBankAccount::from_str("C", "3");

    // And an account list that does not contain that account
    let account_list = vec![
        DomainBankAccount::from_str("A", "1"),
        DomainBankAccount::from_str("B", "2"),
    ];

    // And a synchronizer containing those accounts
    let factory_spy = SpySchedulerFactory::new();
    let synchronizer =
        BankTransactionSynchronizer::new(DummyIngestor, &factory_spy, &account_list.clone().into_iter().collect())
            .unwrap();

    // When synchronize is called
    let err = synchronizer.synchronize(&requested_account).await.unwrap_err();

    // Then the returned error was an Synchronize error
    assert_matches!(err, SynchronizerError::SynchronizeError(..));
}

#[tokio::test]
async fn synchronize__returns_error_when_given_incorrect_account_number_only() {
    // Given a requested account with routing number matching below but unique account number
    let requested_account = DomainBankAccount::from_str("A", "3");

    // And an account list that does not contain that account
    let account_list = vec![
        DomainBankAccount::from_str("A", "1"),
        DomainBankAccount::from_str("B", "2"),
    ];

    // And a synchronizer containing those accounts
    let factory_spy = SpySchedulerFactory::new();
    let synchronizer =
        BankTransactionSynchronizer::new(DummyIngestor, &factory_spy, &account_list.clone().into_iter().collect())
            .unwrap();

    // When synchronize is called
    let err = synchronizer.synchronize(&requested_account).await.unwrap_err();

    // Then the returned error was a SynchronizeError
    assert_matches!(err, SynchronizerError::SynchronizeError(..));
}

#[tokio::test]
async fn synchronize__invokes_scheduler_synchronize_for_valid_account() {
    // Given a requested account
    let requested_account = DomainBankAccount::from_str("A", "1");

    // And an account list that contains the requested account
    let account_list = vec![requested_account.clone()];

    // And a synchronizer containing those accounts
    let factory_spy = UniformReturnSchedulerFactory::new(SpyScheduler::default);
    let synchronizer =
        BankTransactionSynchronizer::new(DummyIngestor, &factory_spy, &account_list.clone().into_iter().collect())
            .unwrap();

    // When synchronize is called
    let result = synchronizer.synchronize(&requested_account).await;

    // Then result was Ok
    assert_matches!(result, Ok(()));

    // And the single scheduler was synchronized once
    let scheduler = factory_spy
        .get_returned_schedulers()
        .get(&requested_account)
        .unwrap()
        .clone();
    assert_eq!(scheduler.get_num_synchronize_calls(), 1);
}

#[tokio::test]
async fn synchronize__does_not_synchronize_accounts_not_asked_for() {
    // Given a requested account
    let requested_account = DomainBankAccount::from_str("A", "1");
    // Given an uninvolved account
    let secondary_account = DomainBankAccount::from_str("B", "2");

    // And an account list that contains both accounts
    let account_list = vec![requested_account.clone(), secondary_account.clone()];

    // And a synchronizer containing those accounts
    let factory_spy = UniformReturnSchedulerFactory::new(SpyScheduler::default);
    let synchronizer =
        BankTransactionSynchronizer::new(DummyIngestor, &factory_spy, &account_list.clone().into_iter().collect())
            .unwrap();

    // When synchronize is called
    synchronizer.synchronize(&requested_account).await.unwrap();

    // Then the secondary account was not synchronized
    let scheduler = factory_spy
        .get_returned_schedulers()
        .get(&secondary_account)
        .unwrap()
        .clone();
    assert_eq!(scheduler.get_num_synchronize_calls(), 0);
}

#[tokio::test]
async fn synchronize__returns_error_from_scheduler() {
    // Given a requested account
    let requested_account = DomainBankAccount::from_str("A", "1");

    // And a factory which will return erroring schedulers
    let factory_spy = UniformReturnSchedulerFactory::new(ErroringScheduler::default);

    // And a synchronizer containing that account and factory
    let account_list = vec![requested_account.clone()];
    let synchronizer =
        BankTransactionSynchronizer::new(DummyIngestor, &factory_spy, &account_list.clone().into_iter().collect())
            .unwrap();

    // When synchronize is called
    let err = synchronizer.synchronize(&requested_account).await.unwrap_err();

    // Then a SynchronizeError is returned
    assert_matches!(err, SynchronizerError::SynchronizeError(..));
}
