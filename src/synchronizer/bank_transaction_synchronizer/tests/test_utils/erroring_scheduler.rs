use crate::scheduler::scheduler_trait::{
    error::{Error, Result},
    Scheduler,
};
use thiserror::Error;

#[derive(Debug, Default, Clone)]
pub struct ErroringScheduler;

#[async_trait::async_trait]
impl Scheduler for ErroringScheduler {
    async fn request_synchronize(&self) -> Result<()> {
        Err(Error::RequestSynchronizeError(Box::new(TestError::Test)))
    }
}

#[derive(Debug, Error)]
pub enum TestError {
    #[error("Test Error")]
    Test,
}
