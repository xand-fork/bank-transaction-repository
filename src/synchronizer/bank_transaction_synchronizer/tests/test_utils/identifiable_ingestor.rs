use crate::ingestor::{error::Error as IngestorError, Ingestor};
use std::sync::atomic::{AtomicU32, Ordering};

/// An Ingestor with extra test functions that allow for the comparison of Ingestor instances.
#[derive(Debug)]
pub struct IdentifiableIngestor(AtomicU32);

impl IdentifiableIngestor {
    pub fn new() -> Self {
        Self(AtomicU32::default())
    }
}

#[async_trait::async_trait]
impl Ingestor for IdentifiableIngestor {
    async fn ingest(&self, _bank_account: &crate::domain::data::DomainBankAccount) -> Result<(), IngestorError> {
        unimplemented!()
    }

    fn test_set_identity(&self, identity: u32) {
        self.0.store(identity, Ordering::SeqCst);
    }

    fn test_get_identity(&self) -> u32 {
        self.0.load(Ordering::SeqCst)
    }
}
