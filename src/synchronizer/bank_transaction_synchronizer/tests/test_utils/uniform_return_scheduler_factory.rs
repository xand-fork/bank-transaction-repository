use crate::{
    domain::data::DomainBankAccount,
    scheduler::{
        factory_trait::{error::Result, SchedulerFactory},
        scheduler_trait::Scheduler,
    },
};
use std::{
    collections::HashMap,
    sync::{Arc, Mutex},
};

/// A `SchedulerFactory` that returns clones of the provided Scheduler.
#[derive(Clone)]
pub struct UniformReturnSchedulerFactory<S: Scheduler + Clone, F: Fn() -> S> {
    scheduler_source: F,
    returned_schedulers: Arc<Mutex<HashMap<DomainBankAccount, S>>>,
}

impl<S: Scheduler + Clone, F: Fn() -> S> UniformReturnSchedulerFactory<S, F> {
    pub fn new(scheduler_source: F) -> Self {
        Self {
            returned_schedulers: Arc::default(),
            scheduler_source,
        }
    }

    pub fn get_returned_schedulers(&self) -> HashMap<DomainBankAccount, S> {
        self.returned_schedulers.lock().unwrap().to_owned()
    }
}

impl<S: Scheduler + Clone, F: Fn() -> S> SchedulerFactory for UniformReturnSchedulerFactory<S, F> {
    type Scheduler = S;

    fn create(
        &self,
        _ingestor: crate::ingestor::IngestionHandle,
        bank_account: &DomainBankAccount,
    ) -> Result<Self::Scheduler> {
        let scheduler = (self.scheduler_source)();
        self.returned_schedulers
            .lock()
            .unwrap()
            .insert(bank_account.clone(), scheduler.clone());
        Ok(scheduler)
    }
}
