pub mod error;

use crate::domain::data::DomainBankAccount;
use error::Result;

#[async_trait::async_trait]
pub trait Synchronizer {
    async fn synchronize(&self, account: &DomainBankAccount) -> Result<()>;
}
