pub mod error;

use crate::{
    domain::data::DomainBankAccount,
    ingestor::Ingestor,
    periodic::{error::Result as PeriodicResult, Periodic, PeriodicHandleImpl},
    scheduler::{
        factory_trait::{error::Error as SchedulerFactoryError, SchedulerFactory},
        scheduler_trait::Scheduler,
    },
    synchronizer::synchronizer_trait::{error::Result as SynchronizerResult, Synchronizer},
};
use error::{Error, Result};
use std::{
    collections::{HashMap, HashSet},
    sync::Arc,
};

/// Orchestrates synchronization of a set of bank accounts. Intended to be used for all accounts at a single bank.
#[derive(Debug)]
pub struct BankTransactionSynchronizer<S: Scheduler> {
    schedulers: HashMap<DomainBankAccount, S>,
}

impl<S: Scheduler> BankTransactionSynchronizer<S> {
    pub fn new<I: Ingestor + Sized + 'static, SF: SchedulerFactory<Scheduler = S>>(
        ingestor: I,
        scheduler_factory: &SF,
        accounts: &HashSet<DomainBankAccount>,
    ) -> Result<Self> {
        let handle: Arc<dyn Ingestor> = Arc::new(ingestor);
        let schedulers = accounts
            .iter()
            .map(|a| scheduler_factory.create(handle.clone(), a).map(|s| (a.clone(), s)))
            .collect::<Result<_, SchedulerFactoryError>>()?;
        Ok(Self { schedulers })
    }
}

#[async_trait::async_trait]
impl<S: Scheduler> Synchronizer for BankTransactionSynchronizer<S> {
    async fn synchronize(&self, account: &DomainBankAccount) -> SynchronizerResult<()> {
        let scheduler = self
            .schedulers
            .get(account)
            .ok_or_else(|| Error::UnknownAccount(account.clone()))?;
        Ok(scheduler.request_synchronize().await.map_err(Into::<Error>::into)?)
    }
}

#[async_trait::async_trait]
impl<S: Scheduler + Periodic> Periodic for BankTransactionSynchronizer<S> {
    type Handle = PeriodicHandleImpl<S::Handle>;

    fn start_periodic_synchronization(&self) -> PeriodicResult<Self::Handle> {
        let handles = self
            .schedulers
            .values()
            .map(Periodic::start_periodic_synchronization)
            .collect::<Result<_, _>>()?;
        Ok(Self::Handle::new(handles))
    }
}

#[cfg(test)]
mod tests;
