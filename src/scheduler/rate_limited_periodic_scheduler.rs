mod coordinated_task;
mod countdown_timer;
mod periodic_task_timer;
mod threaded_periodic_task_timer;

#[cfg(test)]
mod integ_tests;
#[cfg(test)]
mod test_utils;

use crate::{
    domain::data::DomainBankAccount,
    ingestor::{error::Result as IngestorResult, IngestionHandle},
    periodic::{
        error::{Error as PeriodicError, Result as PeriodicResult},
        Periodic,
    },
    scheduler::scheduler_trait::{error::Error as SchedulerError, Scheduler},
};
use coordinated_task::{
    CoordinatedTask, IngestionTask, MemoizedCooldownTask, PeriodicTimerResetTask, SharedTask, UnitTask,
};
use countdown_timer::ClockCountdownTimer;
use periodic_task_timer::PeriodicTaskTimer;
use std::{
    sync::{Arc, Mutex},
    time::Duration,
};
use threaded_periodic_task_timer::{ThreadedPeriodicTaskTimer, ThreadedPeriodicTimerHandle};

type BoxedIngestorCoordinatedTask = Box<dyn CoordinatedTask<Output = IngestorResult<()>>>;

/// A Scheduler which enforces a minimum and approximate maximum time between ingestions for a single bank account.
///
/// Arbitrates between automatic (periodic) and manual synchronizations, so that they collectively abide by the
/// requested cooldown.
///
/// A provided timer is used to gate requests, such that a manual request arriving too close after *either* a manual or
/// automatic synchronization will short-circuit and return the last result.
///
/// If `start_periodic_timer` is called, initiates a background job which will invoke a synchronization if the scheduler
/// is not asked to synchronize for longer than the specified period. If the time between manual syncronization requests
/// is never more than the provided period, no background synchronizations will occur.
pub struct RateLimitedPeriodicScheduler {
    task: SharedTask<
        MemoizedCooldownTask<
            ClockCountdownTimer,
            PeriodicTimerResetTask<ThreadedPeriodicTimerHandle, BoxedIngestorCoordinatedTask>,
        >,
    >,
    periodic_task_timer: Arc<Mutex<Option<ThreadedPeriodicTimerHandle>>>,
    cooldown_duration: Duration,
    periodic_interval: Duration,
}

impl RateLimitedPeriodicScheduler {
    pub fn new(
        cooldown: Duration,
        periodic_interval: Duration,
        ingestor: IngestionHandle,
        bank_account: DomainBankAccount,
    ) -> Self {
        let cooldown_timer = ClockCountdownTimer::new(cooldown);

        let periodic_task_timer = Arc::new(Mutex::new(None));
        let inner_task: PeriodicTimerResetTask<ThreadedPeriodicTimerHandle, BoxedIngestorCoordinatedTask> =
            PeriodicTimerResetTask::new(
                periodic_task_timer.clone(),
                Box::new(IngestionTask::new(ingestor, bank_account)),
            );
        let task = SharedTask::new(MemoizedCooldownTask::create(cooldown_timer, inner_task));

        Self {
            task,
            periodic_task_timer,
            cooldown_duration: cooldown,
            periodic_interval,
        }
    }
}

#[async_trait::async_trait]
impl Scheduler for RateLimitedPeriodicScheduler {
    #[tracing::instrument(skip_all, err, fields(from = "btr"))]
    async fn request_synchronize(&self) -> Result<(), SchedulerError> {
        self.task
            .execute()
            .await
            .map_err(|err| SchedulerError::RequestSynchronizeError(Box::new(err)))
    }
}

#[async_trait::async_trait]
impl Periodic for RateLimitedPeriodicScheduler {
    type Handle = PeriodicTimerHandle;

    /// Starts enforcement of the "maximum" time between synchronizations.
    ///
    /// Returns an error if this timer is already running or if the provided period is less than the instance's
    /// cooldown.
    fn start_periodic_synchronization(&self) -> PeriodicResult<Self::Handle> {
        if self.periodic_interval < self.cooldown_duration {
            return Err(PeriodicError::PeriodLessThanCooldown {
                period: self.periodic_interval,
                cooldown: self.cooldown_duration,
            });
        }

        let mut timer_mutex = self
            .periodic_task_timer
            .lock()
            .expect("Could not start the periodic timer for bank transaction ingestion!");

        if timer_mutex.is_some() {
            return Err(PeriodicError::TimerAlreadyStarted);
        }

        let periodic_timer = ClockCountdownTimer::new(self.periodic_interval);

        let periodic_task_timer = ThreadedPeriodicTaskTimer::new(periodic_timer);
        let timer_handle = periodic_task_timer.start(Box::new(UnitTask::new(self.task.clone())));
        *timer_mutex = Some(timer_handle);

        Ok(PeriodicTimerHandle::new(self.periodic_task_timer.clone()))
    }
}

#[derive(Debug)]
pub struct PeriodicTimerHandle {
    periodic_task_timer: Arc<Mutex<Option<ThreadedPeriodicTimerHandle>>>,
}

impl PeriodicTimerHandle {
    pub fn new(periodic_task_timer: Arc<Mutex<Option<ThreadedPeriodicTimerHandle>>>) -> Self {
        Self { periodic_task_timer }
    }
}

impl Drop for PeriodicTimerHandle {
    fn drop(&mut self) {
        match self.periodic_task_timer.lock() {
            Ok(mut guard) => drop(guard.take()),
            Err(mut guard) => {
                tracing::error!("Unable to lock periodic task timer! Dropping timer in the poisoned mutex.");
                drop(guard.get_mut().take());
            }
        }
    }
}
