use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;
use async_trait::async_trait;

#[derive(Debug, Clone)]
pub struct CallbackTask<F: Fn() -> T, T> {
    callback: F,
}

#[async_trait]
impl<F, T> CoordinatedTask for CallbackTask<F, T>
where
    F: Fn() -> T + Send + Sync,
    T: Clone + Send + Sync,
{
    type Output = T;

    async fn execute(&self) -> Self::Output {
        (self.callback)()
    }
}

impl<F, T> CallbackTask<F, T>
where
    F: Fn() -> T + Send + Sync,
    T: Clone + Send + Sync,
{
    pub fn new(callback: F) -> Self {
        Self { callback }
    }
}
