use crate::{
    domain::data::DomainBankAccount,
    ingestor::{error::Result, Ingestor},
};
use std::sync::{Arc, Mutex};

#[derive(Debug)]
pub struct SpyIngestor {
    ingestion_history: Mutex<Vec<DomainBankAccount>>,
    ingestion_result: Arc<Mutex<Result<()>>>,
}

impl SpyIngestor {
    pub fn get_ingestion_history(&self) -> Vec<DomainBankAccount> {
        self.ingestion_history.lock().unwrap().clone()
    }
    pub fn set_ingestion_result(&self, result: Result<()>) {
        *self.ingestion_result.lock().unwrap() = result;
    }
}

#[async_trait::async_trait]
impl Ingestor for SpyIngestor {
    async fn ingest(&self, bank_account: &crate::domain::data::DomainBankAccount) -> Result<()> {
        self.ingestion_history.lock().unwrap().push(bank_account.clone());
        self.ingestion_result.lock().unwrap().to_owned()
    }
}

impl Default for SpyIngestor {
    fn default() -> Self {
        Self {
            ingestion_history: Mutex::default(),
            ingestion_result: Arc::new(Mutex::new(Ok(()))),
        }
    }
}
