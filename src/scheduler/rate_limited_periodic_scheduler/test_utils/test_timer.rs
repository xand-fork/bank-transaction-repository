use crate::scheduler::rate_limited_periodic_scheduler::countdown_timer::CountdownTimer;
use std::sync::{
    atomic::{AtomicBool, Ordering},
    Arc,
};
use tokio::sync::Notify;

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum TimerState {
    Expired,
    Active,
}

#[derive(Debug, Clone)]
pub struct TestTimer {
    is_expired: Arc<AtomicBool>,
    was_reset: Arc<AtomicBool>,
    reset_notifier: Arc<Notify>,
    waiter_notifier: Arc<Notify>,
}

impl TestTimer {
    pub fn create(initial_state: TimerState) -> Self {
        Self {
            is_expired: Arc::new(AtomicBool::new(initial_state == TimerState::Expired)),
            was_reset: Arc::default(),
            reset_notifier: Arc::default(),
            waiter_notifier: Arc::default(),
        }
    }

    pub fn set_state(&self, state: TimerState) {
        let was_expired = self
            .is_expired
            .swap(state == TimerState::Expired, std::sync::atomic::Ordering::SeqCst);
        if !was_expired && state == TimerState::Expired {
            self.reset_notifier.notify_one();
        }
    }

    fn set_was_reset(&self, was_reset: bool) {
        let prev_was_reset = self.was_reset.swap(was_reset, std::sync::atomic::Ordering::SeqCst);
        if prev_was_reset && !was_reset {
            self.reset_notifier.notify_one();
        }
    }

    pub async fn wait_for_waiter(&self) {
        self.waiter_notifier.notified().await;
    }

    pub fn was_reset(&self) -> bool {
        self.was_reset.load(Ordering::SeqCst)
    }
}

#[async_trait::async_trait]
impl CountdownTimer for TestTimer {
    fn reset(&mut self) {
        self.set_was_reset(true);
        self.set_state(TimerState::Active);
    }

    fn is_expired(&self) -> bool {
        self.is_expired.load(Ordering::SeqCst)
    }

    async fn wait_for_expiration(&self) {
        self.waiter_notifier.notify_one();
        while !self.is_expired() {
            self.reset_notifier.notified().await;
        }
    }
}
