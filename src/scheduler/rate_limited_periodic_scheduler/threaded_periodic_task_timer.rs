use crate::scheduler::rate_limited_periodic_scheduler::{
    countdown_timer::CountdownTimer,
    periodic_task_timer::{PeriodicTaskTimer, PeriodicTaskTimerHandle},
    CoordinatedTask,
};
use futures::FutureExt;
use std::sync::Arc;
use tokio::{sync::Notify, task::JoinHandle};
use tracing::Instrument;

/// A `PeriodicTaskTimer` that spawns a thread and runs the provided task periodically in the background.
/// When started, a timer handle is returned which provides the ability to reset or stop the
/// background task.
pub struct ThreadedPeriodicTaskTimer<Timer: CountdownTimer + Send> {
    timer: Timer,
}

impl<Timer: CountdownTimer + Send> ThreadedPeriodicTaskTimer<Timer> {
    pub fn new(timer: Timer) -> Self {
        Self { timer }
    }

    #[allow(clippy::mut_mut)]
    async fn task(mut timer: Timer, task: Box<dyn CoordinatedTask<Output = ()>>, reset_notify: Arc<Notify>) {
        loop {
            futures::select! {
                _ = timer.wait_for_expiration().fuse() => {
                    timer.reset();
                    async {
                        tracing::info!("PeriodicTaskTriggered");
                        task.execute().await;
                    }
                    .instrument(tracing::info_span!("periodic_task_triggered")).await;
                },
                _ = reset_notify.notified().fuse() => timer.reset(),
            };
        }
    }
}

impl<Timer: CountdownTimer + Send + 'static> PeriodicTaskTimer for ThreadedPeriodicTaskTimer<Timer> {
    type Handle = ThreadedPeriodicTimerHandle;

    fn start(self, task: Box<dyn CoordinatedTask<Output = ()>>) -> Self::Handle {
        let notifier = Arc::new(Notify::new());

        let join_handle = tokio::spawn(Self::task(self.timer, task, notifier.clone()));

        ThreadedPeriodicTimerHandle::new(notifier, join_handle)
    }
}

#[derive(Debug)]
pub struct ThreadedPeriodicTimerHandle {
    reset_notify: Arc<Notify>,
    join_handle: JoinHandle<()>,
}

impl ThreadedPeriodicTimerHandle {
    fn new(reset_notify: Arc<Notify>, join_handle: JoinHandle<()>) -> Self {
        Self {
            reset_notify,
            join_handle,
        }
    }
}

impl PeriodicTaskTimerHandle for ThreadedPeriodicTimerHandle {
    fn reset(&self) {
        self.reset_notify.notify_waiters();
    }

    fn stop(self) {}
}

impl Drop for ThreadedPeriodicTimerHandle {
    fn drop(&mut self) {
        self.join_handle.abort();
    }
}

#[cfg(test)]
mod tests;
