use crate::{
    domain::data::DomainBankAccount,
    ingestor::{error::Error as IngestorError, IngestionHandle},
    scheduler::rate_limited_periodic_scheduler::CoordinatedTask,
};

/// A `CoordinatedTask` which invokes the given ingestor's ingest() method when executed.
pub struct IngestionTask {
    bank_account: DomainBankAccount,
    ingestion_handle: IngestionHandle,
}

impl IngestionTask {
    pub fn new(ingestor: IngestionHandle, bank_account: DomainBankAccount) -> Self {
        Self {
            bank_account,
            ingestion_handle: ingestor,
        }
    }
}

#[async_trait::async_trait]
impl CoordinatedTask for IngestionTask {
    type Output = Result<(), IngestorError>;

    async fn execute(&self) -> Self::Output {
        self.ingestion_handle.ingest(&self.bank_account).await
    }
}
