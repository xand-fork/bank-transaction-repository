use crate::scheduler::rate_limited_periodic_scheduler::CoordinatedTask;
use std::sync::Arc;

/// A `CoordinatedTask` which wraps another task. Once constructed, cloning the `SharedTask` will take a shared reference
/// to the inner Task and thus acts as a clone-able handle to one instance.
pub struct SharedTask<Task: CoordinatedTask> {
    task: Arc<Task>,
}

impl<Task: CoordinatedTask> SharedTask<Task> {
    pub fn new(task: Task) -> Self {
        Self { task: Arc::new(task) }
    }
}

#[async_trait::async_trait]
impl<Task: CoordinatedTask> CoordinatedTask for SharedTask<Task> {
    type Output = Task::Output;

    async fn execute(&self) -> Self::Output {
        self.task.execute().await
    }
}

impl<Task: CoordinatedTask> Clone for SharedTask<Task> {
    fn clone(&self) -> Self {
        Self {
            task: self.task.clone(),
        }
    }
}
