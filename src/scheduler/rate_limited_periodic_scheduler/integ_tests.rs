use crate::{
    domain::data::DomainBankAccount,
    ingestor::{
        bank_client::error::Error as BankClientError, error::Error as IngestorError, spy_bank_client::TestError,
        MockIngestor,
    },
    periodic::{error::Error as PeriodicError, Periodic},
    scheduler::{
        factory_trait::SchedulerFactory,
        rate_limited_periodic_scheduler::test_utils::spy_ingestor::SpyIngestor,
        rate_limited_periodic_scheduler_factory::RateLimitedPeriodicSchedulerFactory,
        scheduler_trait::{error::Error as SchedulerError, Scheduler},
    },
};
use assert_matches::assert_matches;
use std::{sync::Arc, time::Duration};

const ZERO_DURATION: Duration = Duration::ZERO;
const UNIT_DURATION: Duration = Duration::from_secs(60);
const ALMOST_FOREVER: Duration = Duration::from_secs(40 * 3600);

async fn yield_to_advance_time_by(duration: Duration) {
    tokio::time::advance(duration).await;
    tokio::task::yield_now().await;
}

#[tokio::test]
async fn request_synchronize__synchronizes_when_no_cooldown() {
    tokio::time::pause();

    // Given a scheduler factory with no cooldown timer nor period
    let cooldown = ZERO_DURATION;
    let period = ALMOST_FOREVER;
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When we request synchronization
    scheduler.request_synchronize().await.unwrap();

    // Then the ingestor's synchronize method was called once with the original account
    assert_eq!(ingestor.get_ingestion_history(), vec![account]);
}

#[tokio::test]
async fn request_synchronize__synchronize_not_invoked_within_cooldown() {
    tokio::time::pause();

    // Given a short cooldown
    let cooldown = UNIT_DURATION * 2;
    let less_than_cooldown_duration = cooldown - UNIT_DURATION;

    // And a scheduler factory with a very long period and the previous cooldown
    let period = ALMOST_FOREVER;
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When we request synchronization initially
    scheduler.request_synchronize().await.unwrap();

    // And we request synchronization again within the cooldown period
    yield_to_advance_time_by(less_than_cooldown_duration).await;
    scheduler.request_synchronize().await.unwrap();

    // Then the ingestor's synchronize method was called only once
    assert_eq!(ingestor.get_ingestion_history(), vec![account]);
}

#[tokio::test]
async fn request_synchronize__synchronize_can_be_invoked_after_cooldown() {
    tokio::time::pause();

    // Given a short cooldown
    let cooldown = UNIT_DURATION;
    let more_than_cooldown_duration = cooldown + UNIT_DURATION;

    // And a scheduler factory with a very long period and the previous cooldown
    let period = ALMOST_FOREVER;
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When we request synchronization
    scheduler.request_synchronize().await.unwrap();

    // And request synchronization again after the cooldown expires
    yield_to_advance_time_by(more_than_cooldown_duration).await;
    scheduler.request_synchronize().await.unwrap();

    // Then the ingestor's synchronize method was called both times
    assert_eq!(ingestor.get_ingestion_history(), vec![account.clone(), account.clone()]);
}

#[tokio::test]
async fn start_periodic_synchronization__synchronize_invoked_twice_after_one_period_passes() {
    tokio::time::pause();

    // Given no cooldown and a short period duration
    let cooldown = ZERO_DURATION;
    let period_duration = UNIT_DURATION * 2;

    let less_than_period_duration = period_duration - UNIT_DURATION;
    let more_than_period_duration_less_than_two_durations = period_duration + UNIT_DURATION;

    // And a scheduler factory with the previous cooldown and period durations
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period_duration);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When the timer is started
    let _handle = scheduler.start_periodic_synchronization().unwrap();

    // And we let the initial ingestion kick off (periodic timer is reset here)
    yield_to_advance_time_by(less_than_period_duration).await;

    // And slightly more than the period duration passes
    yield_to_advance_time_by(more_than_period_duration_less_than_two_durations).await;

    // Then the ingestor's synchronize method was called twice
    assert_eq!(ingestor.get_ingestion_history().len(), 2);
}

#[tokio::test]
async fn start_periodic_synchronization__synchronize_is_invoked_upon_timer_start_up() {
    tokio::time::pause();

    // Given no cooldown and a short period duration
    let cooldown = ZERO_DURATION;
    let period_duration = UNIT_DURATION * 2;

    let less_than_period_duration = period_duration - UNIT_DURATION;

    // And a scheduler factory with the previous cooldown and period durations
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period_duration);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When the timer is started
    let _handle = scheduler.start_periodic_synchronization().unwrap();

    // And less than the period duration passes
    yield_to_advance_time_by(less_than_period_duration).await;

    // Then the ingestor's synchronize method is called once
    assert_eq!(ingestor.get_ingestion_history().len(), 1);
}

#[tokio::test]
async fn request_synchronize__resets_the_periodic_timer_when_called() {
    tokio::time::pause();

    // Given no cooldown and a short period duration
    let cooldown = ZERO_DURATION;
    let period_duration = UNIT_DURATION * 2;

    let less_than_period_duration = period_duration - UNIT_DURATION;

    // And a scheduler factory with the previous cooldown and period durations
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period_duration);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // And the timer is started
    let _handle = scheduler.start_periodic_synchronization().unwrap();

    // And we let the initial ingestion kick off (periodic timer is reset here)
    yield_to_advance_time_by(less_than_period_duration).await;

    // When we manually invoke request synchronize before the period expires
    yield_to_advance_time_by(less_than_period_duration).await;
    scheduler.request_synchronize().await.unwrap(); // (periodic timer is reset here)

    // And slightly longer than the original period duration (set by the initial ingestion) passes
    // but the reset period (set by the manual ingestion) has not yet expired ->
    // period_duration + less_than_period_duration (above) means time will be slightly more than one
    // period after the initial synchronization
    yield_to_advance_time_by(period_duration).await;

    // Then the ingestor's synchronize method was called only twice
    // Once for the initial automatic sync and once for the manual sync
    assert_eq!(ingestor.get_ingestion_history().len(), 2);
}

#[tokio::test]
async fn request_synchronize__synchronize_invoked_at_originally_scheduled_time_when_cooldown_is_active() {
    tokio::time::pause();

    // Given a short cooldown and a slightly longer period duration
    let cooldown = UNIT_DURATION * 3;
    let period_duration = UNIT_DURATION * 5;

    let less_than_both_cooldown_and_period_duration = UNIT_DURATION;

    // And a scheduler factory with the previous cooldown and period durations
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period_duration);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When the timer is started
    let _handle = scheduler.start_periodic_synchronization().unwrap();

    // And we let the initial ingestion kick off (periodic timer is reset here)
    yield_to_advance_time_by(less_than_both_cooldown_and_period_duration).await;

    // And synchronize is manually invoked again before less than the cooldown and period passes
    yield_to_advance_time_by(less_than_both_cooldown_and_period_duration).await;
    scheduler.request_synchronize().await.unwrap(); // (periodic timer is NOT reset here)

    // And slightly longer than the original period duration (set by the initial ingestion) passes ->
    // period_duration + less_than_both_cooldown_and_period_duration (above) means time will be
    // slightly more than one period after the initial synchronization
    yield_to_advance_time_by(period_duration).await;

    // Then the ingestor's synchronize method was called twice
    // Once for each periodic synchronization (request_synchronize gets the cached value and doesn't
    // reset the timer)
    assert_eq!(ingestor.get_ingestion_history().len(), 2);
}

#[tokio::test]
async fn start_periodic_synchronization__periodic_timer_executes_multiple_times() {
    const NUM_EXPECTED_EXECUTIONS: u32 = 5;
    tokio::time::pause();

    // Given no cooldown and a period duration
    let cooldown = ZERO_DURATION;
    let period_duration = UNIT_DURATION * 2;

    let less_than_period_duration = period_duration - UNIT_DURATION;
    let more_than_period_duration_less_than_two_durations = period_duration + UNIT_DURATION;

    // And a scheduler factory with the previous cooldown and period durations
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period_duration);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When the timer is started
    let _handle = scheduler.start_periodic_synchronization().unwrap();

    // And we let the initial ingestion kick off (periodic timer is reset here)
    yield_to_advance_time_by(less_than_period_duration).await;

    // And slightly over period_duration x (NUM_EXPECTED_EXECUTIONS - 1) time elapses
    for _ in 0..NUM_EXPECTED_EXECUTIONS - 1 {
        // In the test environment, since time passes in chunks, we need to go slightly *more than* the period
        // duration to ensure it triggers.
        yield_to_advance_time_by(more_than_period_duration_less_than_two_durations).await;
        // (periodic timer is reset here)
    }

    // Then the ingestor's synchronize method was called once for each elapsed period
    assert_eq!(
        ingestor.get_ingestion_history(),
        std::iter::repeat(account)
            .take(NUM_EXPECTED_EXECUTIONS as usize)
            .collect::<Vec<_>>()
    );
}

#[tokio::test]
async fn request_synchronize__returns_error_when_ingestion_task_errors() {
    tokio::time::pause();

    // Given a scheduler factory with no cooldown timer and a very long period
    let cooldown = ZERO_DURATION;
    let period = ALMOST_FOREVER;
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    // And an ingestor that errors
    let mut ingestor = MockIngestor::default();
    ingestor
        .expect_ingest()
        .times(1)
        .return_const(Err(IngestorError::BankClientError(Arc::new(
            BankClientError::HistoryFetchError(Box::new(TestError::Test)),
        ))));

    // And a scheduler created from that factory
    let account = DomainBankAccount::test();
    let scheduler = factory.create(Arc::new(ingestor), &account).unwrap();

    // When we request synchronization
    let result = scheduler.request_synchronize().await.unwrap_err();

    // Then the scheduler returns the error returned by the ingestor's synchronize method
    assert_matches!(result, SchedulerError::RequestSynchronizeError(..));
}

#[tokio::test]
async fn start_periodic_synchronization__scheduler_continues_polling_after_errors() {
    tokio::time::pause();

    // Given a scheduler factory with no cooldown timer and a period
    let cooldown = ZERO_DURATION;
    let period = UNIT_DURATION * 2;
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    let less_than_period_duration = period - UNIT_DURATION;
    let more_than_period_duration_less_than_two_durations = period + UNIT_DURATION;

    // And an ingestor that errors
    let ingestor = Arc::new(SpyIngestor::default());
    ingestor.set_ingestion_result(Err(IngestorError::BankClientError(Arc::new(
        BankClientError::HistoryFetchError(Box::new(TestError::Test)),
    ))));

    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // When the timer is started
    let _handle = scheduler.start_periodic_synchronization().unwrap();

    // And we let the initial ingestion kick off (periodic timer is reset here)
    yield_to_advance_time_by(less_than_period_duration).await;

    // And slightly more than two periods elapse
    yield_to_advance_time_by(more_than_period_duration_less_than_two_durations).await; // (periodic timer is reset here)
    yield_to_advance_time_by(more_than_period_duration_less_than_two_durations).await;

    // Then the periodic timer continued to synchronize on schedule. Three calls are expected:
    // from the initial ingestion and and each time after a period passed
    assert_eq!(ingestor.get_ingestion_history().len(), 3);
}

#[tokio::test]
async fn request_synchronize__returns_stored_error_to_manual_call_if_periodic_timer_triggered_error() {
    tokio::time::pause();

    // Given a scheduler factory with a small cooldown and larger period
    let less_than_both_cooldown_and_period_duration = UNIT_DURATION;
    let cooldown = UNIT_DURATION * 3;
    let period = UNIT_DURATION * 5;
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    // And an ingestor that errors
    let mut ingestor = MockIngestor::default();
    ingestor
        .expect_ingest()
        .times(1)
        .return_const(Err(IngestorError::BankClientError(Arc::new(
            BankClientError::HistoryFetchError(Box::new(TestError::Test)),
        ))));

    // And a scheduler created from that factory
    let account = DomainBankAccount::test();
    let scheduler = factory.create(Arc::new(ingestor), &account).unwrap();

    // And the timer is started
    let _handle = scheduler.start_periodic_synchronization().unwrap();

    // And we let the initial ingestion kick off (periodic timer is reset here)
    yield_to_advance_time_by(less_than_both_cooldown_and_period_duration).await;

    // And we request synchronization manually within cooldown
    yield_to_advance_time_by(less_than_both_cooldown_and_period_duration).await;
    let result = scheduler.request_synchronize().await.unwrap_err(); // (periodic timer is NOT reset here)

    // Then the scheduler returns the original error triggered by the timer
    assert_matches!(result, SchedulerError::RequestSynchronizeError(..));
}

#[tokio::test]
async fn start_periodic_synchronization__errors_when_called_more_than_once() {
    tokio::time::pause();

    // Given a scheduler factory with no cooldown timer nor period
    let cooldown = ZERO_DURATION;
    let period = ALMOST_FOREVER;
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor, &account).unwrap();

    // When we call start_periodic_timer twice
    let _handle = scheduler.start_periodic_synchronization().unwrap();
    let result = scheduler.start_periodic_synchronization().unwrap_err();

    // Then the scheduler should return an error
    assert_matches!(result, PeriodicError::TimerAlreadyStarted);
}

#[tokio::test]
async fn start_periodic_synchronization__errors_when_given_cooldown_longer_than_period() {
    tokio::time::pause();

    // Given a period smaller than the cooldown
    let period = UNIT_DURATION;
    let cooldown = UNIT_DURATION * 2;

    // And a scheduler factory with the previous cooldown and period durations
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor, &account).unwrap();

    // When we call start_periodic_timer given the small period
    let error = scheduler.start_periodic_synchronization().unwrap_err();

    // Then the scheduler should return an error
    assert_matches!(error, PeriodicError::PeriodLessThanCooldown { period: actual_period, cooldown: actual_cooldown } if actual_period == period && actual_cooldown == cooldown);
}

#[tokio::test]
async fn stop_periodic_timer__synchronize_not_invoked_when_stopped() {
    tokio::time::pause();

    // Given no cooldown and a short period duration
    let cooldown = ZERO_DURATION;
    let period_duration = UNIT_DURATION * 2;

    let less_than_period_duration = period_duration - UNIT_DURATION;
    let more_than_period_duration_less_than_two_durations = period_duration + UNIT_DURATION;

    // And a scheduler factory with the previous cooldown and period durations
    let factory = RateLimitedPeriodicSchedulerFactory::new(cooldown, period_duration);

    // And a scheduler created from that factory
    let ingestor = Arc::new(SpyIngestor::default());
    let account = DomainBankAccount::test();
    let scheduler = factory.create(ingestor.clone(), &account).unwrap();

    // And the timer is started
    let handle = scheduler.start_periodic_synchronization().unwrap();

    // And we let the initial ingestion kick off (periodic timer is reset here)
    yield_to_advance_time_by(less_than_period_duration).await;

    // When the periodic timer is stopped
    drop(handle);

    // And slightly more than a full period has passed
    yield_to_advance_time_by(more_than_period_duration_less_than_two_durations).await;

    // Then the ingestor's synchronize method is only called once
    assert_eq!(ingestor.get_ingestion_history().len(), 1);
}
