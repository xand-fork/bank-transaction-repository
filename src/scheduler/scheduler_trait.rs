pub mod error;

use error::Result;

#[async_trait::async_trait]
pub trait Scheduler: Send + Sync {
    async fn request_synchronize(&self) -> Result<()>;
}
