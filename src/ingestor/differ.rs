use crate::ingestor::events;
use chrono::{DateTime, Utc};
use serde_json::Value;
use std::fmt::Debug;
use treediff::{
    diff,
    tools::{ChangeType, Recorder},
};

#[cfg(test)]
mod tests;
use crate::domain::data::TimestampedTransaction;
#[cfg(test)]
pub use tests::utils::{get_event_value, parse_time};

#[tracing::instrument(fields(from = "btr"), skip_all)]
pub fn emit_diff_transactions(existing_transaction: TimestampedTransaction, new_transaction: TimestampedTransaction) {
    let v1: Value = serde_json::Value::from(existing_transaction.transaction);
    let v2: Value = serde_json::Value::from(new_transaction.transaction);
    let mut recorder = Recorder::default();
    diff(&v1, &v2, &mut recorder);

    let diffs: TransactionDifferenceDetail =
        format_diff(recorder, existing_transaction.timestamp, new_transaction.timestamp);
    if diffs.total_modifications > 0 {
        tracing::warn!(message = ?events::DifferenceInTransactions{
            diff: &diffs
        });
    }
}

fn format_diff(
    recorder: Recorder<'_, treediff::value::Key, serde_json::Value>,
    original_ingestion_time: DateTime<Utc>,
    current_ingestion_time: DateTime<Utc>,
) -> TransactionDifferenceDetail {
    let mut changes: Vec<(FieldName, FieldChanges)> = vec![];
    let mut total_modifications: u32 = 0;
    for change_type in recorder.calls {
        match change_type {
            ChangeType::Modified(keys, before, after) => {
                changes.push((
                    FieldName {
                        field_name: keys[1].to_string(),
                    },
                    FieldChanges::Modified(ChangeDiff::from_str(
                        before
                            .as_str()
                            .unwrap_or("Error: Could not parse output from difference checker"),
                        after
                            .as_str()
                            .unwrap_or("Error: Could not parse output from difference checker"),
                    )),
                ));
                total_modifications += 1;
            }
            // Do not report on unchanged fields
            // Transactions are strongly typed therefore those with removed or added fields cannot be ingested in the first place  - need not be checked
            ChangeType::Unchanged(..) | ChangeType::Added(_, _) | ChangeType::Removed(_, _) => (),
        };
    }

    TransactionDifferenceDetail {
        original_ingestion_time,
        current_ingestion_time,
        changes,
        total_modifications,
    }
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, PartialOrd, Ord)]
pub struct TransactionDifferenceDetail {
    pub(crate) original_ingestion_time: DateTime<Utc>,
    pub(crate) current_ingestion_time: DateTime<Utc>,
    pub(crate) changes: Vec<(FieldName, FieldChanges)>,
    pub(crate) total_modifications: u32,
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, PartialOrd, Ord)]
pub struct FieldName {
    pub(crate) field_name: String,
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, PartialOrd, Ord)]
pub enum FieldChanges {
    Modified(ChangeDiff),
}

#[derive(Debug, Clone, Hash, Eq, PartialEq, PartialOrd, Ord)]
pub struct ChangeDiff {
    pub(crate) before: String,
    pub(crate) after: String,
}

impl ChangeDiff {
    pub fn from_str(before: &str, after: &str) -> Self {
        Self {
            before: before.to_string(),
            after: after.to_string(),
        }
    }
}
