pub mod utils;

use crate::{
    domain::data::DomainTransaction,
    ingestor::differ::{emit_diff_transactions, tests::utils::get_event_value},
};

use crate::ingestor::differ::{parse_time, TimestampedTransaction};
use insta::assert_display_snapshot;
use tracing_assert_macros::tracing_capture_event_fields;

#[test]
fn emit_diff_transactions__emits_event_when_any_field_changes() {
    // Given some txns
    let previous_metadata = "101";
    let previous_txn = TimestampedTransaction {
        transaction: DomainTransaction::test_with_metadata(previous_metadata),
        timestamp: parse_time("2021-07-09T16:39:57Z"),
    };
    let changed_metadata = "0999";
    let changed_txn = TimestampedTransaction {
        transaction: DomainTransaction::test_with_metadata(changed_metadata),
        timestamp: parse_time("2021-07-11T16:39:57Z"),
    };

    // When
    let events = tracing_capture_event_fields!({
        emit_diff_transactions(previous_txn, changed_txn);
    });

    // Then
    let diff_event: String = get_event_value(events);
    assert_display_snapshot!(diff_event);
}

#[test]
fn emit_diff_transactions__emits_log_when_amounts_changed() {
    // Given some txns
    let previous_amount = "150";
    let previously_ingested_txn = TimestampedTransaction {
        transaction: DomainTransaction::test_with_amount_only(previous_amount),
        timestamp: parse_time("2021-07-09T16:39:57Z"),
    };
    let changed_amount = "15000";
    let changed_bank_txn = TimestampedTransaction {
        transaction: DomainTransaction::test_with_amount_only(changed_amount),
        timestamp: parse_time("2021-07-11T16:39:57Z"),
    };

    // When
    let events = tracing_capture_event_fields!({
        emit_diff_transactions(previously_ingested_txn, changed_bank_txn);
    });

    // Then
    let diff_event: String = get_event_value(events);
    assert_display_snapshot!(diff_event);
}

#[test]
fn emit_diff_transactions__doesnt_emit_log_when_transaction_unchanged() {
    // Given some txns
    let txn = DomainTransaction::test();
    let previously_ingested_txn = TimestampedTransaction {
        transaction: txn.clone(),
        timestamp: parse_time("2021-07-09T16:39:57Z"),
    };
    let newly_ingested_txn = TimestampedTransaction {
        transaction: txn,
        timestamp: parse_time("2021-07-11T16:39:57Z"),
    };

    // When
    let events = tracing_capture_event_fields!({
        emit_diff_transactions(previously_ingested_txn, newly_ingested_txn);
    });

    // Then
    assert_eq!(events.len(), 0);
    let diff_event: String = get_event_value(events);
    assert_display_snapshot!(diff_event);
}
