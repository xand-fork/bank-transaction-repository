use crate::{
    ingestor::bank_client::error::Error as BankClientError,
    store::client::{
        forensics_store::error::Error as ForensicStoreClientError, transaction_store::error::Error as StoreClientError,
    },
};
use std::fmt;
use std::sync::Arc;
use thiserror::Error;

pub type Result<T, E = Error> = std::result::Result<T, E>;

#[allow(clippy::enum_variant_names)]
// Error needs to be Clone so that we can cache the Result returned by the Ingestor and return it
// if another ingestion is requested within the cooldown period. Thus, the inner errors are Arc'd
// to make each variant Clone, as well.
#[derive(Clone, Debug, Error)]
pub enum Error {
    #[error(transparent)]
    BankClientError(Arc<BankClientError>),
    #[error(transparent)]
    TransactionStoreClientError(Arc<StoreClientError>),
    #[error(transparent)]
    ForensicsStoreClientError(Arc<ForensicStoreClientError>),
    #[error(transparent)]
    ForensicsStoreClientErrors(Arc<NewForensicStoreClientErrors>),
}
#[derive(Debug, Error)]
pub struct NewForensicStoreClientErrors(pub Vec<ForensicStoreClientError>);

impl fmt::Display for NewForensicStoreClientErrors {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let mut first = true;
        for item in &self.0 {
            if first {
                write!(f, "{}", item)?;
            } else {
                write!(f, ", {}", item)?;
            }
            first = false;
        }
        Ok(())
    }
}

impl From<BankClientError> for Error {
    fn from(err: BankClientError) -> Self {
        Self::BankClientError(Arc::new(err))
    }
}

impl From<StoreClientError> for Error {
    fn from(err: StoreClientError) -> Self {
        Self::TransactionStoreClientError(Arc::new(err))
    }
}

impl From<ForensicStoreClientError> for Error {
    fn from(err: ForensicStoreClientError) -> Self {
        Self::ForensicsStoreClientError(Arc::new(err))
    }
}
