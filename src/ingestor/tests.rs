use super::{
    clock::MockClock,
    error::Error,
    spy_bank_client::{SpyBankClient, TestError},
    Ingestor, UniqueTransactionIngestor,
};
use crate::{
    domain::data::{BankTransaction, DomainBankAccount, DomainTransaction, TimestampedTransaction},
    ingestor::{
        bank_client::error::Error as BankClientError,
        differ::parse_time,
        events::{BankTransactionsRetrieved, IngestionCompleted, IngestionStarted},
    },
    store::{
        backends::{in_memory_transaction_store::InMemoryTransactionStore, spy_store::SpyStore},
        client::{
            forensics_store::ForensicsStoreClient,
            transaction_store::{insert::error::Error as InsertError, query::TransactionStoreQueryClient},
        },
        TransactionStoreClient,
    },
    TransactionDirection,
};
use assert_matches::assert_matches;
use async_trait::async_trait;
use chrono::{DateTime, Utc};
use std::sync::{Arc, Mutex};
use tokio::runtime::Runtime;
use tracing_assert_core::debug_fmt_ext::DebugFmtExt;
use tracing_assert_macros::tracing_capture_event_fields;
use xand_money::{Money, Usd};

#[derive(Debug)]
struct FakeForensicsStore {
    values: Arc<Mutex<Vec<(String, String)>>>,
}

impl FakeForensicsStore {
    pub fn new() -> Self {
        Self {
            values: Arc::new(Mutex::new(Vec::new())),
        }
    }
}

#[async_trait]
impl ForensicsStoreClient for FakeForensicsStore {
    async fn insert_if_new(
        &self,
        bank_txn_id: String,
        synthetic_id: String,
        _timestamp: &DateTime<Utc>,
    ) -> crate::store::client::forensics_store::error::Result<bool> {
        self.values.lock().unwrap().push((bank_txn_id, synthetic_id));
        Ok(true)
    }
}

impl<S: TransactionStoreClient + Default> UniqueTransactionIngestor<SpyBankClient, S, FakeForensicsStore, MockClock> {
    fn test() -> Self {
        let store = S::default();

        let forensic_store = FakeForensicsStore::new();

        let spy_client = SpyBankClient::default();

        let mock_clock = MockClock::new_with_time(chrono::Utc::now());
        Self::new(spy_client, store, forensic_store, mock_clock)
    }
}

#[tokio::test]
async fn ingest__mock_dependencies_returns_ok() {
    // Given
    let ingestor = UniqueTransactionIngestor::<SpyBankClient, InMemoryTransactionStore, _, MockClock>::test();

    // When
    let res = ingestor.ingest(&DomainBankAccount::test()).await;

    // Then
    assert!(res.is_ok());
}

#[tokio::test]
async fn ingest__returns_err_if_bank_client_errs() {
    // Given
    let err_client = SpyBankClient::default();
    err_client.set_result(Err(BankClientError::HistoryFetchError(Box::new(TestError::Test))));

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, _, _> = UniqueTransactionIngestor {
        client: err_client,
        ..UniqueTransactionIngestor::test()
    };

    // When
    let res = ingestor.ingest(&DomainBankAccount::test()).await;

    // Then
    assert_matches!(res, Err(Error::BankClientError(..)));
}

#[tokio::test]
async fn ingest__returns_err_if_insert_fails() {
    // Given
    let err_store = SpyStore::default();
    let expected_after: DateTime<Utc> = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z")
        .unwrap()
        .into();
    let given: DateTime<Utc> = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z")
        .unwrap()
        .into();
    err_store
        .transaction_client_calls
        .set_insert_result(Err(InsertError::InvalidIngestionTime { expected_after, given }));
    let ingestor: UniqueTransactionIngestor<_, SpyStore, FakeForensicsStore, _> = UniqueTransactionIngestor {
        store: err_store.clone(),
        ..UniqueTransactionIngestor::test()
    };

    // When
    let res = ingestor.ingest(&DomainBankAccount::test()).await;

    // Then
    assert_eq!(err_store.transaction_client_calls.get_insert_calls().len(), 1);
    assert_matches!(res, Err(Error::TransactionStoreClientError(..)));
}

#[tokio::test]
async fn ingest__new_txn_saved_to_storage() {
    // Given

    let bank_client_return = BankTransaction::test();
    let new_txn = DomainTransaction::test();
    let bank_account = DomainBankAccount::test();

    let client = SpyBankClient::default();
    client.set_result(Ok(vec![bank_client_return]));

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            client,
            ..UniqueTransactionIngestor::test()
        };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(ingestor.store.count(&bank_account).await, 1);
    assert!(ingestor
        .store
        .contains_transaction_with_id(&bank_account, &new_txn.get_transaction_id())
        .await
        .unwrap());
}

#[tokio::test]
async fn ingest__existing_bank_txn_is_filtered_out() {
    // Given
    let time = parse_time("2021-07-09T16:39:57Z");
    let bank_txn = BankTransaction::test();
    let txn = TimestampedTransaction {
        transaction: bank_txn.clone().into(),
        timestamp: time,
    };
    let client = SpyBankClient::default();
    client.set_result(Ok(vec![bank_txn]));
    let bank_account = DomainBankAccount::test();

    let store = InMemoryTransactionStore::new_with(bank_account.clone(), vec![txn]);

    let ingestor = UniqueTransactionIngestor {
        client,
        store: store.clone(),
        ..UniqueTransactionIngestor::test()
    };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(store.count(&bank_account).await, 1);
}

#[tokio::test]
async fn ingest__when_bank_client_returns_empty_set_then_storage_is_unchanged() {
    // Given
    let time = parse_time("2021-07-09T16:39:57Z");
    let client = SpyBankClient::default();
    let bank_account = DomainBankAccount::test();
    let txn = TimestampedTransaction {
        transaction: DomainTransaction::test(),
        timestamp: time,
    };
    let store = InMemoryTransactionStore::new_with(bank_account.clone(), vec![txn]);
    let ingestor = UniqueTransactionIngestor {
        client,
        store: store.clone(),
        ..UniqueTransactionIngestor::test()
    };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(store.count(&bank_account).await, 1);
}

#[tokio::test]
async fn ingest__differentiates_between_existing_and_new_txns() {
    // Given
    let time1 = parse_time("2021-07-09T16:39:57Z");
    let time2 = parse_time("2021-07-11T16:39:57Z");
    let existing_bank_txn = BankTransaction::new(
        "0".to_string(),
        Usd::from_str("100.00").unwrap(),
        TransactionDirection::Credit,
        "metadata".to_string(),
    );
    let new_bank_txn = BankTransaction::new(
        "1".to_string(),
        Usd::from_str("120.00").unwrap(),
        TransactionDirection::Credit,
        "metadata".to_string(),
    );
    let existing_txn = TimestampedTransaction {
        transaction: existing_bank_txn.clone().into(),
        timestamp: time1,
    };
    let new_txn = TimestampedTransaction {
        transaction: new_bank_txn.clone().into(),
        timestamp: time2,
    };

    let client = SpyBankClient::default();
    client.set_result(Ok(vec![existing_bank_txn, new_bank_txn]));

    let bank_account = DomainBankAccount::test();

    let store = InMemoryTransactionStore::new_with(bank_account.clone(), vec![existing_txn.clone()]);
    let ingestor = UniqueTransactionIngestor {
        client,
        store: store.clone(),
        ..UniqueTransactionIngestor::test()
    };

    assert_eq!(store.count(&bank_account).await, 1);
    assert!(store
        .contains_transaction_with_id(&bank_account, &existing_txn.transaction.get_transaction_id())
        .await
        .unwrap());

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(store.count(&bank_account).await, 2);
    assert!(store
        .contains_transaction_with_id(&bank_account, &new_txn.transaction.get_transaction_id())
        .await
        .unwrap());
}

#[tokio::test]
async fn ingest__on_success_then_last_ingest_time_updated() {
    // Given
    let ingest_time: DateTime<Utc> = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z")
        .unwrap()
        .into();
    let clock = MockClock::new_with_time(ingest_time);

    let bank_account = DomainBankAccount::new("111111".to_string(), "111111".to_string());
    let other_bank_account = DomainBankAccount::new("222222".to_string(), "222222".to_string());

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            clock,
            ..UniqueTransactionIngestor::test()
        };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(
        ingestor.store.get_last_ingest_time(&bank_account).await.unwrap(),
        Some(ingest_time)
    );
    assert_eq!(
        ingestor.store.get_last_ingest_time(&other_bank_account).await.unwrap(),
        None
    );
}

#[tokio::test]
async fn ingest__on_failure_then_last_ingest_time_not_updated() {
    // Given
    let ingest_time = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z").unwrap();
    let clock = MockClock::new_with_time(ingest_time.into());

    let err_client = SpyBankClient::default();
    err_client.set_result(Err(BankClientError::HistoryFetchError(Box::new(TestError::Test))));

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            client: err_client,
            clock,
            ..UniqueTransactionIngestor::test()
        };
    let bank_account = DomainBankAccount::test();
    let original_time = ingestor.store.get_last_ingest_time(&bank_account).await.unwrap();

    // When
    let res = ingestor.ingest(&bank_account).await;

    // Then
    res.unwrap_err();
    assert_eq!(
        ingestor.store.get_last_ingest_time(&bank_account).await.unwrap(),
        original_time
    );
}

#[tokio::test]
async fn ingest__passes_expected_bank_account_to_storage() {
    // Given
    let expected_bank_account = DomainBankAccount::test();

    let client = SpyBankClient::default();
    client.set_result(Ok(vec![BankTransaction::test()]));

    let store = SpyStore::default();
    store.transaction_client_calls.set_insert_result(Ok(()));

    let ingestor: UniqueTransactionIngestor<_, SpyStore, FakeForensicsStore, _> = UniqueTransactionIngestor {
        client,
        store: store.clone(),
        ..UniqueTransactionIngestor::test()
    };

    // When
    ingestor.ingest(&expected_bank_account).await.unwrap();

    // Then
    assert_eq!(store.transaction_client_calls.get_insert_calls().len(), 1);
    assert_eq!(
        store.transaction_client_calls.get_insert_calls()[0]
            .ingestion_record
            .bank_account,
        expected_bank_account
    );
}

#[tokio::test]
async fn ingest__passes_expected_bank_account_to_bank_client() {
    // Given
    let bank_account = DomainBankAccount::test();
    let client = SpyBankClient::default();

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            client: client.clone(),
            ..UniqueTransactionIngestor::test()
        };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(client.get_history_calls(), vec![bank_account]);
}

#[tokio::test]
async fn ingest__only_saves_new_credit_txns_to_storage() {
    // Given
    let credit_bank_txn = BankTransaction::new(
        "0".to_string(),
        Usd::from_str("100.00").unwrap(),
        TransactionDirection::Credit,
        "metadata".to_string(),
    );
    let debit_bank_txn = BankTransaction::new(
        "1".to_string(),
        Usd::from_str("100.00").unwrap(),
        TransactionDirection::Debit,
        "metadata".to_string(),
    );
    let domain_credit_txn: DomainTransaction = credit_bank_txn.clone().into();
    let domain_debit_txn: DomainTransaction = debit_bank_txn.clone().into();
    let bank_account = DomainBankAccount::test();

    let client = SpyBankClient::default();
    client.set_result(Ok(vec![credit_bank_txn, debit_bank_txn]));

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            client,
            ..UniqueTransactionIngestor::test()
        };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(ingestor.store.count(&bank_account).await, 1);
    assert!(ingestor
        .store
        .contains_transaction_with_id(&bank_account, &domain_credit_txn.get_transaction_id())
        .await
        .unwrap());
    assert!(!ingestor
        .store
        .contains_transaction_with_id(&bank_account, &domain_debit_txn.get_transaction_id())
        .await
        .unwrap());
}

#[test]
fn ingest__emits_event_for_ingestion_started() {
    // Given
    let ingest_time: DateTime<Utc> = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z")
        .unwrap()
        .into();
    let clock = MockClock::new_with_time(ingest_time);
    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            clock,
            ..UniqueTransactionIngestor::test()
        };
    let bank_account = DomainBankAccount::test();

    // When
    let rt = Runtime::new().unwrap();
    let events = tracing_capture_event_fields!({
        rt.block_on(ingestor.ingest(&bank_account)).unwrap();
    });

    // Then
    let expected_event: Vec<(String, String)> = vec![(
        "message".into(),
        IngestionStarted {
            ingestion_time: &ingest_time,
            bank_account: &bank_account,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event), "{:?}", events);
}

#[test]
fn ingest__emits_event_summarizing_transactions_retrieved() {
    // Given
    let time1 = parse_time("2021-07-09T16:39:57Z");
    let bank_account = DomainBankAccount::test();
    let existing_bank_txn = BankTransaction::new(
        "0".to_string(),
        Usd::from_str("100.00").unwrap(),
        TransactionDirection::Credit,
        "metadata".to_string(),
    );
    let new_bank_txn = BankTransaction::new(
        "1".to_string(),
        Usd::from_str("100.00").unwrap(),
        TransactionDirection::Credit,
        "metadata_different_txn".to_string(),
    );

    let existing_txn = TimestampedTransaction {
        transaction: existing_bank_txn.clone().into(),
        timestamp: time1,
    };

    let client = SpyBankClient::default();
    client.set_result(Ok(vec![existing_bank_txn, new_bank_txn]));

    let store = InMemoryTransactionStore::new_with(bank_account.clone(), vec![existing_txn]);
    let ingestor = UniqueTransactionIngestor {
        client,
        store,
        ..UniqueTransactionIngestor::test()
    };

    // When
    let rt = Runtime::new().unwrap();
    let events = tracing_capture_event_fields!({
        rt.block_on(ingestor.ingest(&bank_account)).unwrap();
    });

    // Then
    let expected_event: Vec<(String, String)> = vec![(
        "message".into(),
        BankTransactionsRetrieved {
            total: 2,
            new: 1,
            bank_account: &bank_account,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event), "{:?}", events);
}

#[test]
fn ingest__emits_event_for_ingestion_completed() {
    // Given
    let ingest_time: DateTime<Utc> = chrono::DateTime::parse_from_rfc3339("2021-07-09T16:39:57Z")
        .unwrap()
        .into();
    let clock = MockClock::new_with_time(ingest_time);
    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            clock,
            ..UniqueTransactionIngestor::test()
        };
    let bank_account = DomainBankAccount::test();

    // When
    let rt = Runtime::new().unwrap();
    let events = tracing_capture_event_fields!({
        rt.block_on(ingestor.ingest(&bank_account)).unwrap();
    });

    // Then
    let expected_event: Vec<(String, String)> = vec![(
        "message".into(),
        IngestionCompleted {
            ingestion_time: &ingest_time,
            bank_account: &bank_account,
        }
        .debug_fmt(),
    )];
    assert!(events.contains(&expected_event), "{:?}", events);
}

#[test]
fn ingest__emits_event_on_error() {
    // Given
    let err_client = SpyBankClient::default();
    err_client.set_result(Err(BankClientError::HistoryFetchError(Box::new(TestError::Test))));

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            client: err_client,
            ..UniqueTransactionIngestor::test()
        };

    // When
    let rt = Runtime::new().unwrap();
    let events = tracing_capture_event_fields!({
        rt.block_on(ingestor.ingest(&DomainBankAccount::test())).unwrap_err();
    });

    // Then
    let expected_event: Vec<(String, String)> = vec![(
        "error".into(),
        Error::from(BankClientError::HistoryFetchError(Box::new(TestError::Test))).to_string(),
    )];
    assert!(events.contains(&expected_event), "{:?}", events);
}

#[tokio::test]
async fn ingest__new_txn_added_to_forensic_store() {
    // Given
    let bank_client_return = BankTransaction::test();
    let domain_transaction: DomainTransaction = bank_client_return.clone().into();
    let bank_transaction_id = bank_client_return.bank_provided_id();
    let synthetic_id = domain_transaction.get_transaction_id();
    let bank_account = DomainBankAccount::test();
    let client = SpyBankClient::default();
    client.set_result(Ok(vec![bank_client_return]));
    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            client,
            ..UniqueTransactionIngestor::test()
        };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert!(ingestor
        .forensics_store
        .values
        .lock()
        .unwrap()
        .contains(&(bank_transaction_id, synthetic_id)));
}

#[tokio::test]
async fn ingest__filters_out_duplicate_transactions_from_input() {
    // Given
    let bank_transaction_1 = BankTransaction::test_with_id("1");
    let bank_transaction_2 = BankTransaction::test_with_id("2");
    let new_txn = DomainTransaction::from(bank_transaction_1.clone());
    let bank_account = DomainBankAccount::test();

    let client = SpyBankClient::default();
    // Bank client returns transactions with different bank provided ids and the same synthetic ids
    client.set_result(Ok(vec![bank_transaction_1, bank_transaction_2]));

    let ingestor: UniqueTransactionIngestor<_, InMemoryTransactionStore, FakeForensicsStore, _> =
        UniqueTransactionIngestor {
            client,
            ..UniqueTransactionIngestor::test()
        };

    // When
    ingestor.ingest(&bank_account).await.unwrap();

    // Then
    assert_eq!(ingestor.store.count(&bank_account).await, 1);
    assert!(ingestor
        .store
        .contains_transaction_with_id(&bank_account, &new_txn.get_transaction_id())
        .await
        .unwrap());
}
