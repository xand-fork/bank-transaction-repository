use crate::{
    domain::data::{BankTransaction, DomainBankAccount},
    ingestor::bank_client::{
        error::{Error, Result},
        BankClient,
    },
    BankClientConfiguration,
};
use std::sync::{Arc, Mutex};
use thiserror::Error;

#[derive(Debug, Clone)]
pub struct SpyBankClient {
    config: Option<BankClientConfiguration>,
    stored_results: Arc<Mutex<Result<Vec<BankTransaction>>>>,
    history_calls: Arc<Mutex<Vec<DomainBankAccount>>>,
}

impl SpyBankClient {
    pub fn new_with_config(config: BankClientConfiguration) -> Self {
        Self {
            config: Some(config),
            ..Self::default()
        }
    }

    pub fn get_history_calls(&self) -> Vec<DomainBankAccount> {
        self.history_calls.lock().unwrap().to_owned()
    }

    pub fn set_result(&self, stored_results: Result<Vec<BankTransaction>>) {
        *self.stored_results.lock().unwrap() = stored_results;
    }
}

impl Default for SpyBankClient {
    fn default() -> Self {
        Self {
            config: Option::default(),
            stored_results: Arc::new(Mutex::new(Ok(vec![]))),
            history_calls: Arc::default(),
        }
    }
}

#[async_trait::async_trait]
impl BankClient for SpyBankClient {
    async fn get_transaction_history(&self, bank_account: &DomainBankAccount) -> Result<Vec<BankTransaction>> {
        self.history_calls.lock().unwrap().push(bank_account.clone());
        match self.stored_results.lock().unwrap().as_ref() {
            Ok(txns) => Ok(txns.clone()),
            Err(_) => Err(Error::HistoryFetchError(Box::new(TestError::Test))),
        }
    }

    fn get_config(&self) -> BankClientConfiguration {
        self.config.as_ref().unwrap().clone()
    }
}

#[derive(Debug, Error)]
pub enum TestError {
    #[error("Test Error")]
    Test,
}
