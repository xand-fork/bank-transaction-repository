use std::sync::{Arc, Mutex};
use xand_banks::{
    date_range::DateRange,
    models::{account::TransferRequest, BankBalance, BankTransaction, BankTransferResponse},
    BankAdapter,
};

#[derive(Clone, Debug)]
pub struct HistoryCallRecord {
    pub account_number: String,
    pub date_range: DateRange,
}

#[derive(Clone, Debug)]
pub struct SpyBankAdapter {
    stored_results: Arc<Mutex<xand_banks::Result<Vec<BankTransaction>>>>,
    history_calls: Arc<Mutex<Vec<HistoryCallRecord>>>,
}

impl SpyBankAdapter {
    pub fn get_history_calls(&self) -> Vec<HistoryCallRecord> {
        self.history_calls.lock().unwrap().to_owned()
    }

    pub fn set_result(&self, stored_results: xand_banks::Result<Vec<BankTransaction>>) {
        *self.stored_results.lock().unwrap() = stored_results;
    }
}

impl Default for SpyBankAdapter {
    fn default() -> Self {
        Self {
            stored_results: Arc::new(Mutex::new(Ok(vec![]))),
            history_calls: Arc::default(),
        }
    }
}

#[async_trait::async_trait]
impl BankAdapter for SpyBankAdapter {
    async fn balance(&self, _account_number: &str) -> xand_banks::Result<BankBalance> {
        unimplemented!()
    }

    async fn transfer(
        &self,
        _request: TransferRequest,
        _metadata: Option<String>,
    ) -> xand_banks::Result<BankTransferResponse> {
        unimplemented!()
    }

    async fn history(&self, account_number: &str, date_range: DateRange) -> xand_banks::Result<Vec<BankTransaction>> {
        self.history_calls.lock().unwrap().push(HistoryCallRecord {
            account_number: account_number.to_string(),
            date_range,
        });
        self.stored_results.lock().unwrap().to_owned()
    }

    fn memo_char_limit(&self) -> u32 {
        unimplemented!()
    }
}
