-- Your SQL goes here
CREATE TABLE account (
    id INTEGER PRIMARY KEY NOT NULL,
    routing_number TEXT NOT NULL,
    account_number TEXT NOT NULL,
    UNIQUE (routing_number, account_number)
);

CREATE INDEX idx_account ON account (routing_number, account_number);

CREATE TABLE ingestion (
    id INTEGER PRIMARY KEY NOT NULL,
    account_id INTEGER NOT NULL,
    timestamp INTEGER NOT NULL,
    FOREIGN KEY(account_id) REFERENCES account(id)
);

CREATE INDEX idx_ingestion_account_id_timestamp ON ingestion (account_id, timestamp);

-- Table named 'transaction_record' because 'transaction' is a reserved term in sql
CREATE TABLE transaction_record (
    id INTEGER PRIMARY KEY NOT NULL,
    account_id INTEGER NOT NULL,
    ingestion_id INTEGER NOT NULL,
    transaction_hash_id TEXT NOT NULL,
    amount TEXT NOT NULL,
    credit_or_debit TEXT NOT NULL CHECK (credit_or_debit IN ('credit', 'debit')),
    metadata TEXT NOT NULL,
    FOREIGN KEY(ingestion_id) REFERENCES ingestion(id),
    FOREIGN KEY(account_id) REFERENCES account(id),
    UNIQUE (transaction_hash_id)
);

CREATE INDEX idx_transaction_record_account_id ON transaction_record (account_id);

--Table named 'forensics' for ForensicsStoreClient
CREATE TABLE forensics (
    id INTEGER PRIMARY KEY NOT NULL,
    bank_provided_id TEXT NOT NULL,
    transaction_hash_id TEXT NOT NULL,
    timestamp INTEGER NOT NULL,
    UNIQUE (bank_provided_id, transaction_hash_id)
);
