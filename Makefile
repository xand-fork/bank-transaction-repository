TEMP_DB := "test.sqlite3"

deps:
	./deps.sh
# Utility task to re-generate schema.rs file for diesel
schema:
	rm $(TEMP_DB) || true
	diesel migration run --database-url $(TEMP_DB)

# Utility to nest all use statements
nest:
	cargo +nightly fmt -- --config imports_granularity="Crate"

clippy:
	cargo clippy --all-targets --all-features -- -D warnings

unit-test:
	cargo test --lib

integ-test:
	cargo test --test btr_integ_tests -- --test-threads=1

prod-integ-test:
	./tests/run_mcb_production_test.sh
