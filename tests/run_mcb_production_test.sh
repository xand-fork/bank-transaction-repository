#!/usr/bin/env bash

# Launch a local port mapping to MCB proxy server
gcloud container clusters get-credentials tpfs-beta-member1 --zone us-west1-a --project xand-production
kubectl port-forward -n mcb-proxy deployment/mcb-proxy 3128:3128 &
KUBECTL_PID=$!
sleep 2s

# Set up configuration and strings that contain secrets
./tests/set_up_mcb_secrets.sh

# Run the integration test with the local proxy set
HTTPS_PROXY=127.0.0.1:3128 cargo test --test mcb_production_tests --features prod-integ -- --nocapture
TEST_RESULT=$?

# Stop the proxy server whether the tests passed or failed
kill -15 "${KUBECTL_PID}"

# Report if the local proxy port is unexpectedly still in use
ACTIVE_PORTS=$(netstat -tupln 2>/dev/null | grep ":3128" | wc -l)
if [ $ACTIVE_PORTS -gt 0 ]; then
    echo "Failed to kill 'kubectl' process for proxy port mapping"
fi

# Forward the exit status from the test
exit "$TEST_RESULT"
