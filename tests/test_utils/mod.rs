mod mcb;
mod prov;

use bank_transaction_repository::{
    construct_store_client, BankAccount, BankTransactionRepository, BankTransactionRepositoryConfigurationData,
    SqliteDatabaseClient, StoreConfiguration,
};
use std::{path::PathBuf, process::Command, sync::Arc, time::Duration};
use xand_secrets::{CheckHealthError, ReadSecretError, Secret, SecretKeyValueStore};

pub use mcb::McbActor;
pub use prov::ProvActor;

pub struct FakeSecrets;

#[async_trait::async_trait]
impl SecretKeyValueStore for FakeSecrets {
    async fn read(&self, key: &str) -> Result<Secret<String>, ReadSecretError> {
        Ok(Secret::new(key.to_string()))
    }
    async fn check_health(&self) -> Result<(), CheckHealthError> {
        Ok(())
    }
}

#[async_trait::async_trait]
pub trait BankAccountActor {
    async fn get_balance(&self) -> u128;
    async fn transfer_to(&self, amount: u128, metadata: String, recipient: BankAccount);
}

pub fn initialize_test_btr_with(
    config: &str,
) -> BankTransactionRepository<Arc<SqliteDatabaseClient>, Arc<SqliteDatabaseClient>> {
    let config_path_dir = {
        let mut path = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
        path.push("tests/config");
        path
    };
    let config_path = {
        let mut path = config_path_dir.clone();
        path.push(config);
        path
    };

    let file = std::fs::File::open(config_path).unwrap();
    let config_data: BankTransactionRepositoryConfigurationData = serde_yaml::from_reader(file).unwrap();
    let config = config_data.load(config_path_dir).unwrap();
    match &config.store {
        StoreConfiguration::Sqlite { path } => {
            let client = construct_store_client(path).unwrap();
            BankTransactionRepository::initialize(config, client.clone(), client).unwrap()
        }
    }
}

fn start_bank_mocks() {
    let mut cmd = Command::new("docker-compose");
    cmd.args(["-f", "tests/docker-compose.yaml", "up", "-d"])
        .output()
        .unwrap();
}

fn stop_bank_mocks() {
    let mut cmd = Command::new("docker-compose");
    cmd.args(["-f", "tests/docker-compose.yaml", "down"]).output().unwrap();
}

pub fn delete_database() {
    let mut db_dir = PathBuf::from(env!("CARGO_MANIFEST_DIR"));
    db_dir.push("tests/config/database");
    if db_dir.exists() {
        std::fs::remove_dir_all(db_dir).unwrap_or_default();
    }
}

pub fn create_btr_with_fresh_env() -> BankTransactionRepository<Arc<SqliteDatabaseClient>, Arc<SqliteDatabaseClient>> {
    create_btr_with_fresh_env_with("integ_config.yaml")
}

pub fn create_btr_with_fresh_env_with(
    config: &str,
) -> BankTransactionRepository<Arc<SqliteDatabaseClient>, Arc<SqliteDatabaseClient>> {
    stop_bank_mocks();
    delete_database();
    std::thread::sleep(Duration::from_secs(1));

    start_bank_mocks();
    let btr = initialize_test_btr_with(config);
    std::thread::sleep(Duration::from_secs(1));
    btr
}
