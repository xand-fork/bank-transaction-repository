use crate::test_utils::{BankAccountActor, FakeSecrets};
use bank_transaction_repository::BankAccount;
use num_traits::ToPrimitive;
use std::sync::Arc;
use xand_banks::{
    banks::treasury_prime_adapter::{
        client_wrapper::RealClient,
        config::{TreasuryPrimeAuthConfig, TreasuryPrimeConfig},
        TreasuryPrimeAdapter,
    },
    models::account::TransferRequest,
    xand_money::{Money, Usd},
    BankAdapter,
};

pub struct ProvActor {
    account: BankAccount,
    provident_client: TreasuryPrimeAdapter<RealClient>,
}

impl ProvActor {
    pub fn new(account: BankAccount, url: &str) -> Self {
        let adapter = TreasuryPrimeConfig {
            auth: TreasuryPrimeAuthConfig {
                secret_key_username: "my-fake-provident-username".to_string(),
                secret_key_password: "my-fake-provident-password".to_string(),
            },
            url: url.parse().unwrap(),
            timeout: 60,
        }
        .get_adapter(Arc::new(FakeSecrets))
        .unwrap();

        ProvActor {
            account,
            provident_client: adapter,
        }
    }
}

#[async_trait::async_trait]
impl BankAccountActor for ProvActor {
    async fn get_balance(&self) -> u128 {
        self.provident_client
            .balance(&self.account.get_account_number())
            .await
            .unwrap()
            .available_balance
            .into_minor_units()
            .to_u128()
            .unwrap()
    }

    async fn transfer_to(&self, amount: u128, metadata: String, recipient: BankAccount) {
        self.provident_client
            .transfer(
                TransferRequest {
                    amount: Usd::from_u64_minor_units(amount as u64).expect("Must convert Amount to Usd"),
                    dst_account_number: recipient.get_account_number(),
                    src_account_number: self.account.get_account_number(),
                },
                Some(metadata),
            )
            .await
            .unwrap();
    }
}
