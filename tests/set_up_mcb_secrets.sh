#!/usr/bin/env bash

cat << EOF > tests/config/secrets/prod_secret_store.yaml
mcb-username: "${MCB_TRUST_USERNAME}"
mcb-password: "${MCB_TRUST_PASSWORD}"
mcb-client-app-ident: "TransparentSystems"
mcb-organization-id: "${ORG_ID}"
EOF

cat << EOF > tests/config/mcb_production_config.yaml
banks:
  - routing_number: "121141343"
    account_numbers: ["${TRUST_ACCOUNT}"]
    bank:
      mcb:
        url: "https://cap.mcbankny.com"
        auth:
          secret-key-username: "mcb-username"
          secret-key-password: "mcb-password"
        account-info-manager:
          secret-key-client-app-ident: "mcb-client-app-ident"
          secret-key-organization-id: "mcb-organization-id"
    sync_policy:
      cooldown_timeout_secs: 1
      periodic_synchronization_interval_secs: 3600
store:
  backend: Sqlite
  path: ./database/prod_integ_transaction_store.sqlite3
secret_store:
  local_file:
    yaml-file-path: ./secrets/prod_secret_store.yaml
EOF

cat << EOF > tests/mcb_acct_trn_body.txt
{
  "AcctTrnInqRq":{
    "EFXHdr" : {
      "Version" : "1.2",
      "Client" : {
        "Organization" : {
          "OrgId" : "${ORG_ID}"
        },
        "ClientAppIdent":"TransparentSystems"
      },
      "Tracking" : {
        "OriginatingTrnId" : "5dd7c07efa-5dd7c07efawe34563",
        "TrnId" : "5dd7c07efa-5dd7c07efawe34563",
        "ParentTrnId" : "5dd7c07efa-5dd7c07efawe34563"
      }
    },
    "AcctTrnSel" : {
      "AcctKeys" : {
        "AcctId" : "${TRUST_ACCOUNT}",
        "AcctType" : {
          "AcctTypeValue" : "DDA",
          "AcctTypeSource" : "IFX"
        }
      }
    }
  }
}
EOF