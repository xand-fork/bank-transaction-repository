#![allow(non_snake_case)]

mod test_utils;

use bank_transaction_repository::{BankAccount, HistoryRequest, TransactionDirection};
use chrono::DateTime;
use insta::assert_debug_snapshot;
use std::{sync::Arc, time::Duration};
use test_utils::{create_btr_with_fresh_env, create_btr_with_fresh_env_with, BankAccountActor, McbActor, ProvActor};
use xand_banks::{
    banks::mcb_adapter::{
        config::{McbClientTokenRetrievalAuthConfig, McbConfig, RawAccountInfoManagerConfig},
        CloneableMcbAdapter,
    },
    BankAdapter,
};
use xand_money::{Money, Usd};

use crate::test_utils::FakeSecrets;

const COOLDOWN_TIME: u64 = 1;
const MCB_URL: &str = "http://localhost:8888/metropolitan/";
const PROVIDENT_URL: &str = "http://localhost:8888/provident/";
const MCB_ROUTING: &str = "121141343";
const TRUST_MCB_ACCOUNT: &str = "5555555555";
const PROVIDENT_ROUTING: &str = "211374020";
const TRUST_PROVIDENT_ACCOUNT: &str = "9999999999";
const DATE_IN_THE_PAST: &str = "2019-10-12T07:20:50.52Z";

#[tokio::test]
async fn get_history__returns_transaction_on_second_manual_ingestion_no_periodicity() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env();

    // And a trust account, a dummy account, and a trust mcb actor
    let trust_mcb_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());
    let dummy_account = BankAccount::new(MCB_ROUTING.into(), "0".into());
    let dummy_mcb = McbActor::new(dummy_account.clone(), MCB_URL);

    // And a history request
    let history_request = HistoryRequest::new(
        trust_mcb_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    // An initial credit transaction to the MCB trust account (BTR only stores incoming transactions)
    let amount_1: u64 = 100;
    let metadata_1 = "transfer 1".to_string();
    dummy_mcb
        .transfer_to(amount_1.into(), metadata_1.clone(), trust_mcb_account.clone())
        .await;

    // And the initial pull takes place
    let initial_txns = btr.get_history(&history_request).await.unwrap();

    // Second credit transaction for to the MCB trust account
    let amount_2: u64 = 10000;
    let metadata_2 = "transfer 2".to_string();
    dummy_mcb
        .transfer_to(amount_2.into(), metadata_2.clone(), trust_mcb_account.clone())
        .await;

    // Wait for cooldown
    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));

    // When history is pulled the second time
    let second_pull_txns = btr.get_history(&history_request).await.unwrap();

    // Then no transactions were in the initial history results
    assert_eq!(initial_txns.len(), 0);

    // And only the second transaction was in the second history results
    assert_eq!(second_pull_txns.len(), 1);
    let txn = &second_pull_txns[0];
    assert_eq!(txn.get_amount(), Usd::from_u64_minor_units(amount_2).unwrap());
    assert_eq!(txn.get_metadata(), metadata_2);
    assert_eq!(txn.get_credit_or_debit(), TransactionDirection::Credit);
}

#[tokio::test]
async fn get_history__distinguishes_between_multiple_banks() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env();

    // Accounts
    let trust_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());
    let dummy_account = BankAccount::new(MCB_ROUTING.into(), "0".into());
    let bank = McbActor::new(dummy_account.clone(), MCB_URL);

    let prov_trust_account = BankAccount::new(PROVIDENT_ROUTING.into(), TRUST_PROVIDENT_ACCOUNT.into());
    let prov_dummy_account = BankAccount::new(PROVIDENT_ROUTING.into(), "0".into());
    let prov_bank = ProvActor::new(prov_dummy_account.clone(), PROVIDENT_URL);

    // And a history request
    let mcb_history_request = HistoryRequest::new(
        trust_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );
    let prov_history_request = HistoryRequest::new(
        prov_trust_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    // And the initial pull takes place
    let _ = btr.get_history(&mcb_history_request).await.unwrap();
    let _ = btr.get_history(&prov_history_request).await.unwrap();

    // Second transaction for MCB
    let amount: u64 = 10000;
    let mcb_metadata = "mcb data".to_string();
    bank.transfer_to(amount.into(), mcb_metadata.clone(), trust_account.clone())
        .await;
    let prov_amount: u64 = 20000;
    let prov_metadata = "prov data".to_string();
    prov_bank
        .transfer_to(prov_amount.into(), prov_metadata.clone(), prov_trust_account.clone())
        .await;

    // wait for cooldown
    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));

    // When history is pulled the second time
    let mcb_txns = btr.get_history(&mcb_history_request).await.unwrap();
    let prov_txns = btr.get_history(&prov_history_request).await.unwrap();

    // Verify mcb transaction is returned
    assert_eq!(mcb_txns.len(), 1);
    let mcb_txn = &mcb_txns[0];
    assert_eq!(mcb_txn.get_amount(), Usd::from_u64_minor_units(amount).unwrap());
    assert_eq!(mcb_txn.get_metadata(), mcb_metadata);
    assert_eq!(mcb_txn.get_credit_or_debit(), TransactionDirection::Credit);

    // Verify mcb transaction is returned
    assert_eq!(prov_txns.len(), 1);
    let prov_txn = &prov_txns[0];
    assert_eq!(prov_txn.get_amount(), Usd::from_u64_minor_units(prov_amount).unwrap());
    assert_eq!(prov_txn.get_metadata(), prov_metadata);
    assert_eq!(prov_txn.get_credit_or_debit(), TransactionDirection::Credit);
}

#[tokio::test]
async fn get_history__returns_cached_results_within_cooldown() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env();

    // And a trust account, a dummy account, and a trust mcb actor
    let trust_mcb_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());
    let dummy_account = BankAccount::new(MCB_ROUTING.into(), "0".into());
    let dummy_mcb = McbActor::new(dummy_account.clone(), MCB_URL);

    // And a history request
    let history_request = HistoryRequest::new(
        trust_mcb_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    // And the initial pull takes place
    let initial_txns = btr.get_history(&history_request).await.unwrap();

    // An initial transaction for MCB
    let amount_1: u64 = 100;
    let metadata_1 = "transfer 1".to_string();
    dummy_mcb
        .transfer_to(amount_1.into(), metadata_1.clone(), trust_mcb_account.clone())
        .await;

    // And the second pull takes place after the cooldown
    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));
    let second_pull_txns = btr.get_history(&history_request).await.unwrap();

    // Second transaction for MCB
    let amount_2: u64 = 10000;
    let metadata_2 = "transfer 2".to_string();
    dummy_mcb
        .transfer_to(amount_2.into(), metadata_2.clone(), trust_mcb_account.clone())
        .await;

    // When history is pulled the third time within the cooldown
    let third_pull_txns = btr.get_history(&history_request).await.unwrap();

    // Then no transactions were in the initial history results
    assert_eq!(initial_txns.len(), 0);

    // And the first transaction was the only transaction in both the second and third history results
    assert_eq!(second_pull_txns.len(), 1);
    assert_eq!(third_pull_txns.len(), 1);

    let txn = &second_pull_txns[0];
    assert_eq!(txn.get_amount(), Usd::from_u64_minor_units(amount_1).unwrap());
    assert_eq!(txn.get_metadata(), metadata_1);
    assert_eq!(txn.get_credit_or_debit(), TransactionDirection::Credit);
    assert_eq!(&third_pull_txns[0], txn);
}

#[tokio::test]
async fn get_history__returns_only_transactions_within_date_range() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env();

    // And a trust account, a dummy account, and a trust mcb actor
    let trust_mcb_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());
    let dummy_account = BankAccount::new(MCB_ROUTING.into(), "0".into());
    let dummy_mcb = McbActor::new(dummy_account.clone(), MCB_URL);

    // And a history request
    let initial_history_request = HistoryRequest::new(
        trust_mcb_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    // And the initial pull takes place
    let _ = btr.get_history(&initial_history_request).await.unwrap();

    // An initial transaction for MCB
    let amount_1: u64 = 100;
    let metadata_1 = "transfer 1".to_string();
    dummy_mcb
        .transfer_to(amount_1.into(), metadata_1.clone(), trust_mcb_account.clone())
        .await;

    // And the second pull takes place after the cooldown
    let previous_ingestion_time = chrono::Utc::now();
    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));
    let _ = btr.get_history(&initial_history_request).await.unwrap();

    // Second transaction for MCB
    let amount_2: u64 = 20000;
    let metadata_2 = "transfer 2".to_string();
    dummy_mcb
        .transfer_to(amount_2.into(), metadata_2.clone(), trust_mcb_account.clone())
        .await;

    // When history is pulled the third time within the cooldown
    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));
    let new_history_request = HistoryRequest::new(trust_mcb_account.clone(), previous_ingestion_time);
    let latest_txn = btr.get_history(&new_history_request).await.unwrap();

    // Then only latest of two transactions is returned
    assert_eq!(latest_txn.len(), 1);

    let txn = &latest_txn[0];
    assert_eq!(txn.get_amount(), Usd::from_u64_minor_units(amount_2).unwrap());
    assert_eq!(txn.get_metadata(), metadata_2);
    assert_eq!(txn.get_credit_or_debit(), TransactionDirection::Credit);
}

#[tokio::test]
async fn get_history__errors_because_of_bad_bank_auth() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env_with("bad_secrets_integ_config.yaml");

    // And a trust account, a dummy account, and a trust mcb actor
    let trust_mcb_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());

    // And a history request
    let history_request = HistoryRequest::new(
        trust_mcb_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    // And the initial pull takes place
    let res = btr.get_history(&history_request).await;

    // Then
    let err = res.unwrap_err();
    assert_debug_snapshot!("get_history__errors_because_of_bad_bank_auth", err)
}

#[tokio::test]
async fn get_history__errors_because_of_unreachable_bank_url() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env_with("bad_url_integ_config.yaml");

    // And a trust account, a dummy account, and a trust mcb actor
    let trust_mcb_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());

    // And a history request
    let history_request = HistoryRequest::new(
        trust_mcb_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    // And the initial pull takes place
    let res = btr.get_history(&history_request).await;

    // Then
    let err = res.unwrap_err();
    assert_debug_snapshot!("get_history__errors_because_of_unreachable_bank_url", err)
}

#[tokio::test]
async fn get_history__returns_transactions_on_first_call_because_periodic_synchronizer_performed_first_call() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env();

    // And periodic synchronization is kicked off
    let _handle = btr.start_periodic_synchronization().unwrap();
    tokio::time::sleep(Duration::from_secs(1)).await;

    // And a trust account, a dummy account, and a trust actor for both mcb and bank prov
    let trust_mcb_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());
    let dummy_mcb_account = BankAccount::new(MCB_ROUTING.into(), "0".into());
    let dummy_mcb = McbActor::new(dummy_mcb_account.clone(), MCB_URL);

    let trust_prov_account = BankAccount::new(PROVIDENT_ROUTING.into(), TRUST_PROVIDENT_ACCOUNT.into());
    let dummy_prov_account = BankAccount::new(PROVIDENT_ROUTING.into(), "0".into());
    let dummy_prov = ProvActor::new(dummy_prov_account.clone(), PROVIDENT_URL);

    // And a history request for both mcb and bank prov
    let mcb_history_request = HistoryRequest::new(
        trust_mcb_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );
    let prov_history_request = HistoryRequest::new(
        trust_prov_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    // An initial transaction for mcb and bank prov
    let amount: u64 = 10000;
    let mcb_metadata = "mcb transfer".to_string();
    dummy_mcb
        .transfer_to(amount.into(), mcb_metadata.clone(), trust_mcb_account.clone())
        .await;

    let prov_amount: u64 = 20000;
    let prov_metadata = "prov transfer".to_string();
    dummy_prov
        .transfer_to(prov_amount.into(), prov_metadata.clone(), trust_prov_account.clone())
        .await;

    // And the initial manual pulls take place
    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));
    let mcb_txns = btr.get_history(&mcb_history_request).await.unwrap();
    let prov_txns = btr.get_history(&prov_history_request).await.unwrap();

    // Then the transaction was in the respective history results
    assert_eq!(mcb_txns.len(), 1);
    let mcb_txn = &mcb_txns[0];
    assert_eq!(mcb_txn.get_amount(), Usd::from_u64_minor_units(amount).unwrap());
    assert_eq!(mcb_txn.get_metadata(), mcb_metadata);
    assert_eq!(mcb_txn.get_credit_or_debit(), TransactionDirection::Credit);

    assert_eq!(prov_txns.len(), 1);
    let prov_txn = &prov_txns[0];
    assert_eq!(prov_txn.get_amount(), Usd::from_u64_minor_units(prov_amount).unwrap());
    assert_eq!(prov_txn.get_metadata(), prov_metadata);
    assert_eq!(prov_txn.get_credit_or_debit(), TransactionDirection::Credit);
}

#[tokio::test]
async fn start_periodic_synchronization__returns_error_when_called_twice() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env();

    // And periodic synchronization is started
    let _handle = btr.start_periodic_synchronization().unwrap();

    // When start_periodic_synchronization is called again
    let res = btr.start_periodic_synchronization();

    // Then
    let err = res.unwrap_err();
    assert_debug_snapshot!("start_periodic_synchronization__returns_error_when_called_twice", err)
}

#[tokio::test]
async fn start_periodic_synchronization__returns_error_when_period_is_less_than_cooldown() {
    // Given a btr with a fresh database and set of bank mocks
    let btr = create_btr_with_fresh_env_with("period_less_than_cooldown_config.yaml");

    // When periodic synchronization is started
    let res = btr.start_periodic_synchronization();

    // Then
    let err = res.unwrap_err();
    assert_debug_snapshot!(
        "start_periodic_synchronization__returns_error_when_period_is_less_than_cooldown",
        err
    )
}

fn mcb_client() -> CloneableMcbAdapter {
    let mcb_config = McbConfig {
        url: MCB_URL.parse().unwrap(),
        refresh_if_expiring_in_secs: None,
        auth: McbClientTokenRetrievalAuthConfig {
            secret_key_username: "my-fake-metropolitan-username".to_string(),
            secret_key_password: "my-fake-metropolitan-password".to_string(),
        },
        account_info_manager: RawAccountInfoManagerConfig {
            secret_key_client_app_ident: "client_app_ident".to_string(),
            secret_key_organization_id: "organization_id".to_string(),
        },
        timeout: 60,
    };
    let secret_store = FakeSecrets;
    CloneableMcbAdapter::new(&mcb_config, Arc::new(secret_store)).unwrap()
}

#[tokio::test]
async fn transactions_are_identified_by_synthetic_id_rather_than_bank_provided_id() {
    // Given a transaction in the history
    let btr = create_btr_with_fresh_env();

    let trust_account = BankAccount::new(MCB_ROUTING.into(), TRUST_MCB_ACCOUNT.into());
    let dummy_account = BankAccount::new(MCB_ROUTING.into(), "0".into());
    let bank = McbActor::new(dummy_account.clone(), MCB_URL);

    let mcb_history_request = HistoryRequest::new(
        trust_account.clone(),
        DateTime::parse_from_rfc3339(DATE_IN_THE_PAST).unwrap().into(),
    );

    let _prime_the_pump = btr.get_history(&mcb_history_request).await.unwrap();

    let amount: u64 = 10000;
    let metadata = "mcb data".to_string();
    bank.transfer_to(amount.into(), metadata.clone(), trust_account.clone())
        .await;

    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));

    // When a transaction with the same synthetic ID but different bank-provided ID is ingested
    bank.transfer_to(amount.into(), metadata.clone(), trust_account.clone())
        .await;

    std::thread::sleep(Duration::from_secs(COOLDOWN_TIME));

    let bank_txns = mcb_client()
        .history(TRUST_MCB_ACCOUNT, xand_banks::date_range::DateRange::last_24_hours())
        .await
        .unwrap();
    assert_eq!(bank_txns.len(), 2);
    assert_ne!(bank_txns[0].bank_unique_id, bank_txns[1].bank_unique_id);

    // Then the recently ingested transaction does not appear as new in the history
    let btr_txns = btr.get_history(&mcb_history_request).await.unwrap();

    assert_eq!(btr_txns.len(), 1);

    let txn = &btr_txns[0];
    assert_eq!(txn.get_amount().into_u64_minor_units().unwrap(), amount);
    assert_eq!(txn.get_metadata(), metadata);
    assert_eq!(txn.get_credit_or_debit(), TransactionDirection::Credit);
}
