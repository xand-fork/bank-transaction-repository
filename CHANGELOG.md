# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

<!-- When a new release is made, update tags and the URL hyperlinking the diff, see end of document -->
<!-- You may also wish to list updates that are made via MRs but not yet cut as part of a release -->

<!-- When a new release is made, add it here
e.g. ## [0.1.2] - 2020-11-09 -->

## [0.14.2] - 2022-03-14 π
### Changed
- First Party dependencies bumped for xand-banks, tracing-assert-core, tracing-assert-macros,
- Third Party dependencies bumped for regex, tokio


## [0.13.2] - 2021-12-07
### Added
- Integration test that verifies MCB txn history aligns with BTR's DB.

## [0.13.1] - 2021-11-24
### Changed
- Updated the xand-banks dependency that adds a TCP connection timeout.

## [0.13.0] - 2021-11-16

### Changed
- Renamed `contains()` to  `contains_transaction_with_id()`. This function now takes just the id as a parameter instead of the whole transaction.

## [0.12.0] - 2021-11-15

### Added
- Supports emitting user logs when encountering inconsistent transactions during ingestion from third-party bank APIs.

### Removed
- Replaced `get_txns_by_id()` and `get_ingestion_times_for_txns()` with `get_timestamped_txns_by_id()` to minimize database calls.

## [0.11.1] - 2021-11-12

### Added

- Ability to lookup ingestion time(s) by transaction id(s) using `get_ingestion_times_for_txns()`.

## [0.11.0] - 2021-11-10

### Changed
- Ingestor only persists "credit" (money incoming) transactions. BTR will only be used for creates, as of now, so we
  don't want to persist transactions relating to redeems.

## [0.10.1] - 2021-11-10

### Added
- Ability to get ingested transaction(s) by their unique id using `get_txns_by_id()`

## [0.10.0] - 2021-11-05

### Removed
- `raw_data` field on `Transaction` type and in database schema.

## [0.9.0] - 2021-11-04

### Added
- `TransactionStoreInsertClient`, a client for writing data to the store.
- `TransactionStoreQueryClient`, a client for reading data from the store.
- `HistoryPuller` trait that declares a `get_history` function to be implemented by the `TransactionHistoryProvider`.

### Changed
- The `TransactionStoreClient` had its functionality split into `TransactionStoreInsertClient` and
  `TransactionStoreQueryClient` for writing and reading data respectively. TransactionStoreClient is now a trait that
  requires the implementation of both.
- `Periodic` was made fallible.
- `RateLimitedPeriodicScheduler` now `impl`'s `Periodic` instead of the now removed `PeriodicScheduler`.
- Error types are no longer `Clone` except for `crate::ingestor::error::Error`.
- Errors refactored to apply Transparent Systems' [Error Handling Strategy](https://gitlab.com/TransparentIncDevelopment/docs/transparent-engineering-error-handling-strategy/-/tree/master/).

### Removed
- `PeriodicScheduler`, replaced by `Periodic`.

## [0.8.2] - 2021-11-01

### Added
- Diff functionality for comparing transaction fields and logging inconsistencies: `emit_diff_transactions()`
